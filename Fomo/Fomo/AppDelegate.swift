//
//  AppDelegate.swift
//  Fomo
//
//  Created by Khoa Bui on 6/30/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
import Alamofire
import AlamofireNetworkActivityIndicator
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var userInfo: [AnyHashable: Any]?

    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "google.com")

    // MARK: -
    func enablePushNotification(_ enable: Bool = true) {
        if !enable {
            Messaging.messaging().unsubscribe(fromTopic: "fomo_news")
        }
        else {
            Messaging.messaging().subscribe(toTopic: "fomo_news")
        }
    }
    
    // MARK: -
    func isNetworkAvailable () -> Bool {
        return (self.reachabilityManager?.isReachable)!
    }
    
    func startTrackingNetwork () {
        
        self.reachabilityManager?.listener = { status in
            if status == .notReachable {
                self.showNoNetworkConnection()
            }
            else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NetworkReachable"), object: nil)
            }
        }
        
        self.reachabilityManager?.startListening()
    }
    
    func showNoNetworkConnection () {
        UIAlertController.show(in: (self.window?.rootViewController)!, withTitle: "Error", message: NETWORK_LOST, cancelButtonTitle: nil, otherButtonTitles: ["OK"], tap: nil)
    }
    
    // MARK: -
    func createMenuView() {
        
        // Create viewController code...
        
        let dashboardVC = FMDashboardViewController.newInstance()
        
        let menuVC = FMenuViewController.initWithStoryboard()
        
        let nvc = BaseNavigationController(rootViewController: dashboardVC)
        
//        UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
        let slideMenuController = VSlideMenuController(mainViewController:nvc, rightMenuViewController: menuVC)
        
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.startTrackingNetwork()
        
//        self.createMenuView()
        // Override point for customization after application launch.
        FMLogging.setupLogging()
        
        USER_DEFAULT_SET(value: "Roboto", key: .appFont)
        
        FirebaseApp.configure();
        
        registerNotification(application: application)
        
        if let userInfo = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable : Any] {
            self.userInfo = userInfo
        }
        

        Fabric.with([Crashlytics.self])

        NetworkActivityIndicatorManager.shared.isEnabled = true
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//        LAYOUT_MANAGER.handlePushNotification(withUserInfo: USER_DEFAULT_GET(key: "Khoadeptrai") as! [String: Any])
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        if let refreshedToken = InstanceID.instanceID().token() {
            USER_DEFAULT_SET(value: refreshedToken, key: .deviceToken);
            USER_DEFAULT_SYNC();
//            let keychain = Keychain(service: KEYCHAIN_SERVICE);
//            //Should request update push token again in case user has re-enable push from disabled state.
            if USER_DEFAULT_GET(key: .userToken) != nil {
//                API_MANAGER.requestUpdateToken()
            }
            
            self.enablePushNotification()
        }
        
        Messaging.messaging()
            .setAPNSToken(deviceToken, type: MessagingAPNSTokenType.unknown)
        
//        #if DEBUG
//            Messaging.messaging()
//                .setAPNSToken(deviceToken, type: MessagingAPNSTokenType.prod)
//        #else
//            Messaging.messaging()
//                .setAPNSToken(deviceToken, type: MessagingAPNSTokenType.sandbox)
//        #endif
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        if application.applicationState == .inactive || application.applicationState == .background {
            // App in BG
            // Dectect app have already show incomming call view
            //Temporarily do nothing
        }
        else {
            
        }
        
        self.handleReceivePushNotificationWithUserInfo(userInfo: userInfo)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[""] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        self.handleReceivePushNotificationWithUserInfo(userInfo: userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    private func registerNotification(application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {(granted, error) in
                    if (granted)
                    {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                    else{
                        //Do stuff if unsuccessful...
                        print("grant error, try next time")
                    }
            })
            let action = UNNotificationAction(identifier: "Notification1", title: "Expiration notification", options: [.foreground])
            let category = UNNotificationCategory(identifier: "Notification1", actions: [action], intentIdentifiers: [], options: [])
            UNUserNotificationCenter.current().setNotificationCategories([category])
            
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
            UNUserNotificationCenter.current().delegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
    }
    
    func tokenRefreshNotification(notification: NSNotification) {
        print("TokenRefresh")
        if let refreshedToken = InstanceID.instanceID().token() {
            refreshToken(newToken: refreshedToken)
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    // [END refresh_token]
    
    // [START connect_to_fcm]
    func connectToFcm() {
    
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    
    
    private let DEVICE_TOKEN_UPDATE_MAX_TRY = 5;//Max number of re-try attempts to update Firebase push token
    
    
    /// Update Firebase push token to server
    ///
    /// - Parameters:
    ///   - token: the new token
    ///   - counter: counter for retry in case network request fails, will stop retrying until counter reaches DEVICE_TOKEN_UPDATE_MAX_TRY
    func updatePushToken(_ token: String, counter:Int = 0) {
        //If re-try attemp was performed DEVICE_TOKEN_UPDATE_MAX_TRY times, don't try any more.
        if counter > DEVICE_TOKEN_UPDATE_MAX_TRY {
            return;
        }
        
    }

    func refreshToken(newToken: String) {
        
        logInfo(newToken)
        
        USER_DEFAULT_SET(value: newToken, key: .deviceToken);
        USER_DEFAULT_SYNC();
        
        if USER_DEFAULT_GET(key: .userToken) != nil {
//            API_MANAGER.requestUpdateToken()
        }
        
        self.enablePushNotification()
    }
    
}
extension AppDelegate: MessagingDelegate {
    func application(received remoteMessage: MessagingRemoteMessage) {
        //TODO: Handle receive message from Clound message
        print("===========Received message from firebase delegate when in foreground=========")
        if let aps = remoteMessage.appData["aps"] as? [String:Any] {
            print("aps object in push: \(aps)")
            if let alert = aps["alert"] as? [String:Any] {
                
            }
            if let key = aps["customKey"] as? [String:Any] {
                
            }
        }
    }
    
    /// This method will be called whenever FCM receives a new, default FCM token for your
    /// Firebase project's Sender ID.
    /// You can send this token to your application server to send notifications to this device.
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        refreshToken(newToken: fcmToken)
    }
}
//MARK: Handle push notification in iOS 10
extension AppDelegate: UNUserNotificationCenterDelegate {
    @available(iOS 10.0, *)
    
    //MARK: Handle push when notification is tapped while app's in background
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let dict = response.notification.request.content.userInfo;
        self.handleReceivePushNotificationWithUserInfo(userInfo: dict)
        completionHandler()
    }
    
    @available(iOS 10.0, *)
    
    //MARK: Handle push in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler(UNNotificationPresentationOptions.sound)
        let dict = notification.request.content.userInfo;
//        self.handleReceivePushNotificationWithUserInfo(userInfo: dict)
    }
    
    //MARK:- Private method
    func handleReceivePushNotificationWithUserInfo(userInfo: [AnyHashable : Any]) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: userInfo, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            // here "decoded" is of type `Any`, decoded from JSON data
            print("json push object: \(decoded)")
            if let data = decoded as? [String: Any]{
                USER_DEFAULT_SET(value: decoded, key: "Khoadeptrai")
                LAYOUT_MANAGER.handlePushNotification(withUserInfo: data)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}


