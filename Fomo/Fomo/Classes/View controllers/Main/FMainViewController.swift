//
//  FMMainViewController.swift
//  Fomo
//
//  Created by Khoa Bui on 6/30/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class FMMainViewController: BaseViewController {

    static func initWithStoryboard() -> FMMainViewController{
        let viewController = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMMainViewController") as! FMMainViewController
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

