//
//  FMenuViewController.swift
//  Fomo
//
//  Created by Khoa Bui on 7/3/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class FMenuViewController: UIViewController {
    
    let services: [FMServiceType] = [.agb, .privacy, .impressum, .contact]
    var sections = [(FMMenuSection.dashboard, false), (FMMenuSection.news, false) , (FMMenuSection.service, false), (FMMenuSection.settings, false)]
    var categories = FMCategory.getCategories()
    
    static func initWithStoryboard() -> FMenuViewController{
        let viewController = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMenuViewController") as! FMenuViewController
        return viewController
    }
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func menuButtonClicked(_ sender: Any) {
        self.closeRight()
    }
}

extension FMenuViewController {
    fileprivate func setupView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.separatorStyle = .none
    }
}

//extension FMenuViewController : FMLeftMenuHeaderViewDelegate {
//    func leftMenuHeaderView(_ view: FMLeftMenuHeaderView, didClickCollapse collapsed: Bool) {
//        let section = view.tag
//        self.sections[section].1 = collapsed
//        self.tableView.beginUpdates()
//        self.tableView.reloadSections([section], with: .automatic)
//        tableView.endUpdates()
//    }
//}

extension FMenuViewController : FMLeftMenuTableViewCellDelegate {
    func leftMenuCell(_ cell: FLeftMenuTableViewCell, didClickCollapse collapsed: Bool) {
        let indexPath = self.tableView.indexPath(for: cell)
        if let section = indexPath?.section {
            // Toggle collapse
            self.sections[section].1 = collapsed
            self.tableView.beginUpdates()
            self.tableView.reloadSections([section], with: .automatic)
            tableView.endUpdates()
        }
    }
}
extension FMenuViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 0
    }
 
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = FMLeftMenuHeaderView.viewFromNib() as! FMLeftMenuHeaderView
//        headerView.titleLabel.text = self.sections[section].0.rawValue.uppercased()
//        headerView.isCollapsed = self.sections[section].1
//        headerView.tag = section
//        headerView.delegate = self
//        headerView.collapseButton.isHidden = (self.sections[section] == self.sections.first!) ? true : false
//        headerView.isUserInteractionEnabled = (self.sections[section] != self.sections.last!) ? true : false
//        return headerView
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.sections[section].0 == .dashboard || self.sections[section].0 == .settings{
            return 1
        }
        
        if self.sections[section].0 == .news {
            return (self.sections[section].1) ? 1 : self.categories.count + 1
        }
        
        return (self.sections[section].1) ? 1 : self.services.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "dashboardCell", for: indexPath) as! FLeftMenuTableViewCell
            cell.selectionStyle = .gray
            cell.titleLabel.text = self.sections[indexPath.section].0.rawValue
            
            cell.collapseButton.isHidden =  false
            if self.sections[indexPath.section].0 == .dashboard || self.sections[indexPath.section].0 == .settings{
                cell.collapseButton.isHidden = true
            }
            
            if self.sections[indexPath.section].0 == .service{
                cell.selectionStyle = .none
            }
            
            cell.isCollapsed = self.sections[indexPath.section].1
            cell.delegate = self
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "leftMenuCell", for: indexPath) as! FLeftMenuTableViewCell
            cell.selectionStyle = .gray
            if indexPath.section == 2 {
                cell.titleLabel.text = self.services[indexPath.row - 1].stringValue
            }
            else {
                cell.titleLabel.text = categories[indexPath.row - 1].name
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainNAV = self.slideMenuController()?.mainViewController as! BaseNavigationController
        var mainVC = mainNAV.viewControllers.first
        if indexPath.row == 0 {
            
            if self.sections[indexPath.section].0 == .settings{
                let settingsVC = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMSettingsViewController") as! FMSettingsViewController

                self.closeRight()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    self.slideMenuController()?.mainViewController?.present(settingsVC, animated: true, completion: nil)
                })
            }
            else{
                let vc = FMDashboardViewController.newInstance()
                vc.isDashboard = (self.sections[indexPath.section].0 == .news) ? false : true
                let dashboardNAV = BaseNavigationController(rootViewController: vc)
                self.slideMenuController()?.changeMainViewController(dashboardNAV, close: true)
                
//                if mainVC is FMDashboardViewController && mainNAV.viewControllers.count == 1{
//                    (mainVC as! FMDashboardViewController).isDashboard = false
//                    self.closeRight()
//                }
//                else {
//                    let vc = FMDashboardViewController.newInstance()
//                    vc.isDashboard = false
//                    let dashboardNAV = BaseNavigationController(rootViewController: vc)
//                    self.slideMenuController()?.changeMainViewController(dashboardNAV, close: true)
//                }
            }
        }
        else if self.sections[indexPath.section].0 == .news {
            if mainVC is FMHomeViewController && mainNAV.viewControllers.count == 1{
                self.closeRight()
            }
            else {
                let vc = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMHomeViewController")
                let homeNAV = BaseNavigationController(rootViewController: vc)
                self.slideMenuController()?.changeMainViewController(homeNAV, close: true)
                mainVC = vc
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                (mainVC as? FMHomeViewController)?.displayCategoryAtIndex(indexPath.row - 1)
            })
        }
        else if self.sections[indexPath.section].0 == .service {
            let serviceVC = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMServiceViewController") as! FMServiceViewController
            serviceVC.serviceType = self.services[indexPath.row - 1]
            self.closeRight()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: { 
                self.slideMenuController()?.mainViewController?.present(serviceVC, animated: true, completion: nil)
            })
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 2 && indexPath.row == 0 {
            return nil
        }
        return indexPath
    }
}
