//
//  FMDashboardViewController.swift
//  Fomo
//
//  Created by Tuan Nguyen on 8/2/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import MJRefresh
import DZNEmptyDataSet

class FMDashboardViewController: BaseViewController {
    
    static func newInstance() -> FMDashboardViewController{
        let viewController = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMDashboardViewController") as! FMDashboardViewController
        return viewController
    }
    
    // IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    // Properties
    var isDashboard: Bool = true
    var heightAtIndexPath:NSMutableDictionary!
    var layoutDatas: [FMDashboard]?
    let header = MJRefreshNormalHeader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.loadDashboardData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK:- Private method
extension FMDashboardViewController {
    fileprivate func setupView() {
        
        // Set refreshing with target
        header.setRefreshingTarget(self, refreshingAction: #selector(FMDashboardViewController.refreshData))
        // add header to table view
        self.tableView.mj_header = header
        
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 120
        self.heightAtIndexPath = NSMutableDictionary()
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.separatorStyle = .none
        
        self.tableView.register(UINib(nibName: "FMSliderTableViewCell", bundle: nil), forCellReuseIdentifier: "sliderCell")
        self.tableView.register(UINib(nibName: "HeadlineTableViewCell", bundle: nil), forCellReuseIdentifier: "componentHeadlineCell")
        self.tableView.register(UINib(nibName: "FArticlesTableViewCell", bundle: nil), forCellReuseIdentifier: "smallArticlesCell")
        self.tableView.register(UINib(nibName: "FBigArticlesTableViewCell", bundle: nil), forCellReuseIdentifier: "articlesCell")
        self.title = self.isDashboard ? FMMenuSection.dashboard.rawValue : FMMenuSection.news.rawValue
//        setTitleView(title: self.isDashboard ? FMMenuSection.dashboard.rawValue : FMMenuSection.news.rawValue)
        self.showRightButton()
        self.showLeftButton()
    }
    
    fileprivate func setFont() {
        
    }
    
    fileprivate func loadDashboardData() {
        if self.isDashboard {
            self.layoutDatas = LAYOUT_MANAGER.dataLayout[DASHBOARD_TYPE]
        } else {
            self.layoutDatas = LAYOUT_MANAGER.dataLayout[NEWS_TYPE]
        }
        self.tableView.reloadData()
    }
    
    // Selector method
 
    func refreshData(){
//        print("Start refesh")
        LAYOUT_MANAGER.createLayoutDashboard(layout: .Dashboard) { [weak self](iSuccess) in
            if let weakSelf = self {
                weakSelf.tableView.mj_header.endRefreshing()
                weakSelf.loadDashboardData()
            }
        }
    }
}

//MARK:- DZNEmptyDataSetSource, DZNEmptyDataSetDelegate

extension FMDashboardViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "NO DATA FOUND"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }

    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "Please try again !!!"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func buttonImage(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> UIImage! {
        return UIImage(named: "refesh")
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        button.rotateButton(animationDuration: 1, inSecond: 10)
        self.refreshData()
    }
}

extension FMDashboardViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        if let layoutDatas = self.layoutDatas {
            return layoutDatas.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let layoutDatas =  self.layoutDatas {
            guard let count = layoutDatas[section].articles?.count else { return 0 }
            if count == 0 {
                return 0
            } else {
                if layoutDatas[section].layout == .slider {
                    return 2
                }
                return 1 + count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let layoutDatas = self.layoutDatas?[indexPath.section]
        let newIndexRow = (indexPath.row != 0) ? (indexPath.row - 1) : 0
        let articleData = layoutDatas?.articles?[newIndexRow]
        if let layoutType = layoutDatas?.layout, let articleData = articleData, let count = layoutDatas?.articles?.count {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "componentHeadlineCell", for: indexPath) as! HeadlineTableViewCell
                cell.setData(data: layoutDatas)
                cell.isUserInteractionEnabled = false
                return cell
            } else {
                switch layoutType {
                case .slider:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell", for: indexPath) as! FMSliderTableViewCell
                    cell.setData(data: layoutDatas)
                    cell.didSelectArticle = {[weak self](article) -> () in
                        if let weakSelf = self, let article = article as? FMArticle {
                            let articleDetailsVC = FMArticleDetailsViewController.initWithStoryboard()
                            articleDetailsVC.article = article
                            weakSelf.navigationController?.pushViewController(articleDetailsVC, animated: true)
                        }
                    }
                    return cell
                case .single:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "articlesCell", for: indexPath) as! FArticlesTableViewCell
                    
                    cell.setData(articleData, isFullLine: indexPath.row == count )
                    cell.showThumbnail(type: .single, isShow: layoutDatas?.showPicture ?? false)
                    return cell
                case .list:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "smallArticlesCell", for: indexPath) as! FArticlesTableViewCell
                    cell.setData(articleData, isFullLine: indexPath.row == count)
                    cell.showThumbnail(type: .list, isShow: layoutDatas?.showPicture ?? false)
                    return cell
                }
            }
        } else {
            return tableView.dequeueReusableCell(withIdentifier: "componentHeadlineCell")!
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = self.heightAtIndexPath.object(forKey: indexPath) as?  CGFloat {
            return height
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let height = cell.frame.size.height
        self.heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if Connectivity.isNetworkAvailable {
            if let article = self.layoutDatas?[indexPath.section].articles?[indexPath.row - 1] {
                let articleDetailsVC = FMArticleDetailsViewController.initWithStoryboard()
                articleDetailsVC.article = article
                self.navigationController?.pushViewController(articleDetailsVC, animated: true)
            }
        }
        else {
            UIApplication.currentAppDelegate().showNoNetworkConnection()
        }
        tableView.deselectRow(at: indexPath, animated: true)

        
       
    }
    
}


