//
//  BaseViewController.swift
//  Fomo
//
//  Created by Khoa Bui on 6/30/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit


class BaseViewController: UIViewController {
    
    //MARK:- Property
    private var leftButton: UIBarButtonItem!
    private var rightButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- Public method
    
    func showRightButton() {
//        if navigationController?.viewControllers.first == self {
//            // Show left menu
//            rightButton = UIBarButtonItem(image: UIImage(named: "ic_menu_black"), style: .done, target: self, action: #selector(self.rightMenuButtonClicked))
//        } else {
//            rightButton = UIBarButtonItem(image: UIImage(named: "left_arrow"), style: .done, target: self, action: #selector(self.rightMenuButtonClicked))
//        }
        rightButton = UIBarButtonItem(image: UIImage(named: "ic_menu_black"), style: .done, target: self, action: #selector(self.rightMenuButtonClicked))
        
        self.navigationItem.rightBarButtonItem = rightButton
        self.changeNavigationBarToDefaultStyle()
    }
    
    func showLeftButton() {
        leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "logo_small"), style: .done, target: self, action: #selector(self.leftMenuButtonClicked))
        self.navigationItem.leftBarButtonItem = leftButton

        self.changeNavigationBarToDefaultStyle()
    }
    
    func changeNavigationBarToDefaultStyle() {
        
        let attributes:NSDictionary = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName : UIFont.font(type: .Bold, size: 18)]
        self.navigationController?.navigationBar.titleTextAttributes = attributes as? [String : AnyObject]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    
    func setTitleView(title: String) {
        let label = UILabel()
        let attributes:[String : Any] = [NSForegroundColorAttributeName: UIColor.black, NSFontAttributeName : UIFont.font(type: .Bold, size: 18)]
        
        label.attributedText = NSAttributedString(string: title, attributes: attributes)
        label.textAlignment = .center
        label.sizeToFit()
        let titleView = UIView()
        titleView.addSubview(label)
        titleView.frame = label.bounds
        
        
        self.navigationItem.titleView = titleView
    }
    
    func changeNavigationBarToTransparentStyle() {
        
        let attributes:NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes as? [String : AnyObject]
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    }
    
    func setNavigationBarTitleColor(_ color: UIColor) {
        let attributes:NSDictionary = [NSForegroundColorAttributeName: color]
        self.navigationController?.navigationBar.titleTextAttributes = attributes as? [String : AnyObject]
    }
    
    func hideLeftButton() {
        self.navigationItem.leftBarButtonItem = nil
    }
    
    func hideRightButton() {
        self.navigationItem.rightBarButtonItem = nil
    }
    
    func rightMenuButtonClicked() {
        self.slideMenuController()?.toggleRight()
        self.view.endEditing(true)
//        if navigationController?.viewControllers.first == self {
//            self.slideMenuController()?.toggleRight()
//        } else {
//            _ = self.navigationController?.popViewController(animated: true)
//        }
    }

    func leftMenuButtonClicked() {
        self.view.endEditing(true)
    }
    
    func textKeyWordChanged(_ textField: UITextField) {
        
    }
}
