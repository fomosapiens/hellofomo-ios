//
//  FMHeaderBannerView.swift
//  Fomo
//
//  Created by Khoa Bui on 7/26/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class FMHeaderBannerView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    var didSelectArticle: CallBackEvent?
    var article: FMArticle?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFont()
    }
    
    func setData(data: FMArticle?) {
        self.article = data
        self.dateLabel.text = data?.availableDateText() ?? ""
        self.descriptionLabel.text = data?.title ?? ""
        self.thumbImageView.displayImage(data?.primaryImage, placeholderImage: PLACEHOLDER_IMAGE)
    }

    fileprivate func setFont() {
        self.dateLabel.font = UIFont.font(type: .Regular, size: 17)
        self.descriptionLabel.font = UIFont.font(type: .Bold, size: 15)
    }
    
    @IBAction func selectBannerAction(_ sender: Any) {
        self.didSelectArticle?(self.article)
    }
}
