//
//  FArticlesViewController.swift
//  Fomo
//
//  Created by Khoa Bui on 7/3/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import MJRefresh
import DZNEmptyDataSet

class FMArticlesViewController: BaseViewController {

    static func initWithStoryboard() -> FMArticlesViewController{
        let viewController = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMArticlesViewController") as! FMArticlesViewController
        return viewController
    }
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    // IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    // Properties
    var heightAtIndexPath: NSMutableDictionary =  NSMutableDictionary()
    var category: FMCategory!
    var articles: [FMArticle] = []
    
    var currentPage  =  1
    var hasLoaded = false
    var layoutDatas: [FMDashboard]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setupView()
        
        self.loadDashboardData()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if category.id == -1 {
            self.loadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        logInfo("FMArticlesViewController deinit")
    }
    
    // MARK: - Get articles
    func loadData () {
        if category.id == -1 {
            self.displayNews(DB_MANAGER.favoriteNews)
        }
//        else {
//            let id = (category.id == 0) ? "All" : String(category.id!)
//            if let news = DB_MANAGER.getNews(forCategory: id) {
//                self.displayNews(news)
//            }
//            else {
//                self.refreshData()
//            }
//        }
    }
    
    
    
    func displayNews(_ news : [FMArticle]) {
        self.articles = news
        self.loadingActivityIndicator.isHidden = true
        self.hasLoaded = true
        self.tableView.reloadData()
    }
    
    // MARK: - pull-down to refresh data
    func refreshData () {

        self.currentPage = 1

        self.getArticleList { (articles) in
            if let articles = articles {
                self.articles.removeAll()
                self.articles.append(contentsOf: articles)
                let id = (self.category.id == 0) ? "All" : String(self.category.id!)
                DB_MANAGER.removeNews(forCategory: id)
                DB_MANAGER.cacheNews(articles, forCategory: id)
            }

            self.hasLoaded = true
            self.tableView.reloadData()
            self.tableView.mj_header.endRefreshing()
        }
        
        
    }
    
    // MARK: - pull up to loadmore
//    func loadMoreData () {
//
//        self.currentPage += 1
//
//        self.getArticleList { (articles) in
//            if let articles = articles {
//                self.articles.append(contentsOf: articles)
//                self.tableView.reloadData()
//                let id = (self.category.id == nil) ? "All" : String(self.category.id!)
//                DB_MANAGER.cacheNews(articles, forCategory: id)
//
//            }
//            else {
//                self.currentPage -= 1
//            }
//
//            self.tableView.mj_footer.endRefreshing()
//        }
//    }
    
    func getArticleList(withCompletion completion: @escaping ([FMArticle]?) -> Void) {
        if category.isFavorites() {
            self.loadingActivityIndicator.isHidden = true
            self.displayNews(DB_MANAGER.favoriteNews)
        }
        else {
            let id = (category.id == 0) ? nil : String(self.category.id!)
            API_MANAGER.requestGetNewsListing(ofDashboard: nil, categories: id, tags: nil, limit: 50, page: self.currentPage, success: { (articles, dashboard) in
                completion(articles)
                self.loadingActivityIndicator.isHidden = true
            }, failure: { (error) in
                self.loadingActivityIndicator.isHidden = true
                completion(nil)
            })
        }
    }
    
    func loadDashboardData() {
        guard let id = category.id else { return }
        switch id {
        case -1:
            layoutDatas = nil
        case 0:
            self.loadingActivityIndicator.isHidden = true
            layoutDatas = LAYOUT_MANAGER.dataLayout[NEWS_TYPE]
        default:
            LAYOUT_MANAGER.getCategoryData(id) { [weak self] (isSuccess) in
                guard let strongSelf = self else { return }
                strongSelf.loadingActivityIndicator.isHidden = true
                strongSelf.layoutDatas = LAYOUT_MANAGER.dataLayout[String(id)]
                strongSelf.tableView.reloadData()
            }
            //
        }
        self.tableView.reloadData()
    }
    
}

//MARK:- Private method
extension FMArticlesViewController {
    fileprivate func setupView() {
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 120
        self.tableView.tableFooterView =  UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: 1))
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.separatorStyle = .none
        self.tableView.register(UINib(nibName: "FMSliderTableViewCell", bundle: nil), forCellReuseIdentifier: "sliderCell")
        self.tableView.register(UINib(nibName: "HeadlineTableViewCell", bundle: nil), forCellReuseIdentifier: "componentHeadlineCell")
        self.tableView.register(UINib(nibName: "FArticlesTableViewCell", bundle: nil), forCellReuseIdentifier: "smallArticlesCell")
        self.tableView.register(UINib(nibName: "FBigArticlesTableViewCell", bundle: nil), forCellReuseIdentifier: "articlesCell")
        
        if !self.category.isFavorites(){
            self.tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [weak self] in
                if let weakSelf = self {
                    weakSelf.refreshData()
                }
            })
//
//            self.tableView.mj_footer = MJRefreshBackNormalFooter(refreshingBlock: {  [weak self] in
//                if let weakSelf = self {
//                    weakSelf.loadMoreData()
//                }
//            })
            
        }
    }
}

extension FMArticlesViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "NO POSTS FOUND"
        let attrs = [NSFontAttributeName: UIFont.font(type: .Regular, size: 16)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return self.hasLoaded
    }
}


extension FMArticlesViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if let layoutDatas = layoutDatas {
            return layoutDatas.count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let layoutDatas =  self.layoutDatas {
            guard let count = layoutDatas[section].articles?.count else { return 0 }
            if count == 0 {
                return 0
            } else {
                if layoutDatas[section].layout == .slider {
                    return 2
                }
                return 1 + count
            }
        }
        return self.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let layoutDatas = layoutDatas {
            let layoutData = layoutDatas[indexPath.section]
            let newIndexRow = (indexPath.row != 0) ? (indexPath.row - 1) : 0
            let articleData = layoutData.articles?[newIndexRow]
            if let layoutType = layoutData.layout, let articleData = articleData, let count = layoutData.articles?.count {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "componentHeadlineCell", for: indexPath) as! HeadlineTableViewCell
                    cell.setData(data: layoutData)
                    cell.isUserInteractionEnabled = false
                    return cell
                } else {
                    switch layoutType {
                    case .slider:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell", for: indexPath) as! FMSliderTableViewCell
                        cell.setData(data: layoutData)
                        cell.didSelectArticle = {[weak self](article) -> () in
                            if let weakSelf = self, let article = article as? FMArticle {
                                let articleDetailsVC = FMArticleDetailsViewController.initWithStoryboard()
                                articleDetailsVC.article = article
                                weakSelf.navigationController?.pushViewController(articleDetailsVC, animated: true)
                            }
                        }
                        return cell
                    case .single:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "articlesCell", for: indexPath) as! FArticlesTableViewCell
                        cell.setData(articleData, isFullLine: indexPath.row == count)
                        cell.showThumbnail(type: .single, isShow: layoutData.showPicture ?? false)
                        return cell
                    case .list:
                        let cell = tableView.dequeueReusableCell(withIdentifier: "smallArticlesCell", for: indexPath) as! FArticlesTableViewCell
                        cell.setData(articleData, isFullLine: indexPath.row == count)
                        cell.showThumbnail(type: .list, isShow: layoutData.showPicture ?? false)
                        return cell
                    }
                }
            } else {
                return tableView.dequeueReusableCell(withIdentifier: "componentHeadlineCell")!
            }
        }
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "smallArticlesCell", for: indexPath) as! FArticlesTableViewCell
        
        cell.setData(articles[indexPath.row], isFullLine: indexPath.row == articles.count)
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return 120.0
//    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = self.heightAtIndexPath.object(forKey: indexPath) as?  CGFloat {
            return height
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let height = cell.frame.size.height
        self.heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.category.isFavorites(){
            let articleDetailsVC = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMArticleDetailsViewController") as! FMArticleDetailsViewController
            articleDetailsVC.article = articles[indexPath.row]
            self.navigationController?.pushViewController(articleDetailsVC, animated: true)
            
        }
        else if Connectivity.isNetworkAvailable {
            if let article = self.layoutDatas?[indexPath.section].articles?[indexPath.row - 1] {
                let articleDetailsVC = FMArticleDetailsViewController.initWithStoryboard()
                articleDetailsVC.article = article
                self.navigationController?.pushViewController(articleDetailsVC, animated: true)
            }
        }
        else {
            UIApplication.currentAppDelegate().showNoNetworkConnection()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
