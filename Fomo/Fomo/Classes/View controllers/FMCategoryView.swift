//
//  FMCategoryView.swift
//  Fomo
//
//  Created by Khoa Bui on 7/7/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

protocol FMCategoryViewDelegate: class {
    func categoryView(_ view: FMCategoryView , didSelectCategory category: FMCategory?)
}

class FMCategoryView: UIView {
    
    @IBOutlet weak var maxWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoryButton: CustomButton!
    weak var  delegate: FMCategoryViewDelegate?
    var category: FMCategory?
    static func initWithCategory(_ category: FMCategory) -> FMCategoryView {
        let view = FMCategoryView.viewFromNib() as! FMCategoryView
        view.category = category
        
        return view
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFont()
        categoryButton.setDeactiveColor()
        self.maxWidthConstraint.constant = SCREEN_WIDTH()/2.5
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.categoryButton.setTitle(self.category?.name, for: .normal)
    }
    fileprivate func setFont() {
        self.categoryButton.titleLabel?.font = UIFont.font(type: .Regular, size: 17)
    }
    
    @IBAction func selectedCategoryAction(_ sender: Any) {
        self.delegate?.categoryView(self, didSelectCategory: self.category)
    }
    
    
}
