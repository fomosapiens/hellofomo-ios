//
//  FMGalleryCollectionViewCell.swift
//  Fomo
//
//  Created by Tuan Nguyen on 9/19/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class FMGalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        self.imageView.clipsToBounds = true
    }
    
    func bindData(_  url: String) {
        self.imageView.sd_setImage(with: URL(string: url), placeholderImage: PLACEHOLDER_IMAGE)
    }
}
