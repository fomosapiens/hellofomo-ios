//
//  FMAuthorsView.swift
//  Fomo
//
//  Created by Tuan Nguyen on 9/19/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit



class FMAuthorsView: UIView {
    
    static let itemWidth: CGFloat = (SCREEN_WIDTH() - minimumInteritemSpacing - 30)/2
    static let itemHeight: CGFloat = itemWidth - 10
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
         //commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
         //commonInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        commonInit()
    }
    
    
    func commonInit() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
//        layout.itemSize = CGSize(width: FMAuthorsView.itemWidth, height: FMAuthorsView.itemHeight)
//        //                    layout.itemSize = CGSize(width: width, height: width - 10)
//        layout.minimumLineSpacing = FMAuthorsView.minimumLineSpacing
//        layout.minimumInteritemSpacing = FMAuthorsView.minimumInteritemSpacing
//
        self.collectionView.setCollectionViewLayout(layout, animated: true)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
