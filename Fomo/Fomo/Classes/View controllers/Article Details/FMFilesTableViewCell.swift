//
//  FMFilesTableViewCell.swift
//  Fomo
//
//  Created by Tuan Nguyen on 9/19/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol FMFilesTableViewCellDelegate : class {
    func filesTableViewCell(_ cell: FMFilesTableViewCell, didClickUtilityButton withData: (String, Int))
}

class FMFilesTableViewCell: UITableViewCell {

    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var utilityButton: UIButton!
    var fileURL : String = ""
    var newsId: Int = 0

    @IBOutlet weak var progressView: MBProgressHUD!
    weak var delegate : FMFilesTableViewCellDelegate?
    override func awakeFromNib() {
        self.nameLabel.font = UIFont.font(type: .Bold, size: 14)
        self.configProgressView()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.lineView.isHidden = false
    }
    
    func configProgressView() {
        self.progressView.margin = 10
        self.progressView.mode = .annularDeterminate
        self.progressView.bezelView.backgroundColor = UIColor.clear
        self.progressView.bezelView.color = UIColor.clear
        self.progressView.bezelView.style = .solidColor
        self.progressView.backgroundView.color = UIColor.clear
        self.progressView.backgroundView.style = .solidColor
        self.progressView.contentColor = UIColor.colorWithHexString("DF5614")
        self.progressView.removeFromSuperViewOnHide = true
        self.progressView.show(animated: true)
    }
    
    func bindData(_ url: String, newsId: Int, isBottomlineHidden: Bool = false) {
        self.fileURL = url
        self.newsId = newsId
        let name = (url as NSString).lastPathComponent
        self.nameLabel.text = name
        self.lineView.isHidden = isBottomlineHidden
        
        if DOWNLOAD_MANAGER.isDownloading(url) {
            self.bringSubview(toFront: self.progressView)
            let progress = DOWNLOAD_MANAGER.downloadingProgress[url]
            self.updateDownloadProgress(progress)
        }
        else {
            self.bringSubview(toFront: self.utilityButton)
            let image = DOWNLOAD_MANAGER.isDownloaded(url, newsId: newsId) ?  #imageLiteral(resourceName: "ic_file_download") : #imageLiteral(resourceName: "ic_file_download")
            self.utilityButton.setImage(image, for: .normal)
        }
    }
    
    func updateDownloadProgress(_ progress: Float?) {
        if let progress = progress {
            self.bringSubview(toFront: self.progressView)
            self.progressView.progress = progress
        }
    }
    
    @IBAction func utilityButtonClicked(_ sender: Any) {
        self.delegate?.filesTableViewCell(self, didClickUtilityButton: (self.fileURL, newsId))
    }
}
