//
//  FMArticleDetailsViewController.swift
//  Fomo
//
//  Created by Tuan Nguyen on 7/31/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import SDWebImage
import DTCoreText
//import Kanna
import SwiftSoup
//import Fuzi
import FTPopOverMenu_Swift
import INSPhotoGallery

class FMArticleDetailsViewController: BaseViewController {
    
    static func initWithStoryboard() -> FMArticleDetailsViewController{
        let viewController = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMArticleDetailsViewController") as! FMArticleDetailsViewController
        return viewController
    }

    var article : FMArticle?
    var articleDetail: FMArticleDetails?
    
    var articleId: Int?
    
    var relatedPosts : [FMArticle] = []
    var photos : [String] = []
    var authors: [FMAuthor] = []
    var files: [String] = []
    var previewPath : String?
    var fontSizes: [FMFontSizes] = [.normal, .big, .extraBig]
    var fontSizeTitles: [String] {
        return fontSizes.map { font in
            return font.rawValue
        }
    }
    
    var appFontSize: CGFloat = (USER_DEFAULT_GET(key: .appFontSize) as? CGFloat) ?? 18
    // MARK: - Bind data to all sections if needed
    let sectionNameHeight : CGFloat = 35
    let padding : CGFloat = 20
    let authorPadding: CGFloat = 30
    
    var totalSectionHeight: CGFloat = 0
    

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    
//    @IBOutlet weak var introductionLabel: UILabel!
    @IBOutlet weak var introductionTextView: UITextView!
    
    @IBOutlet weak var introductionHeightCnstr: NSLayoutConstraint!
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var loadingView: UIView!
    

    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var stackviewHeightCnstr: NSLayoutConstraint!
    @IBOutlet weak var bottomStackView: UIStackView!
    
    var catNameLabel : UILabel!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.bringSubview(toFront: self.loadingView)
        
//        self.contentTextView.font = UIFont.font(type: .Regular, size: 17)
        self.contentTextView.isScrollEnabled = false
        
        self.dateLabel.font = UIFont.font(type: .Light, size: appFontSize + 3)
        self.titleLabel.font = UIFont.font(type: .Bold, size: appFontSize + 4)
//        self.introductionLabel.font = UIFont.font(type: .Light, size: appFontSize + 3)
        self.titleLabel.numberOfLines = 0
        self.titleLabel.lineBreakMode = .byWordWrapping
        
        
        let id = (self.articleId) ?? self.article?.id
        if let id = id {
            
            API_MANAGER.requestGetNewsDetail(id: id, success: { [weak self](articleDetails) in
                if let articleDetails = articleDetails, let weakSelf = self {
                    weakSelf.articleDetail = articleDetails
                    
                    if weakSelf.article == nil {
                        weakSelf.displayArticleInfo(articleDetails)
                    }
                    else {
                        weakSelf.article!.url = articleDetails.url
                    }
                    weakSelf.preparePhotos(articleDetails.galleries)
                    weakSelf.prepareAuthor(articleDetails.authors)
                    weakSelf.prepareRelatedPosts(articleDetails.relatedNews)
                    weakSelf.prepareFiles(articleDetails.attachments)
                    
                    weakSelf.setupSections(articleDetails)


                    weakSelf.initNavigationButtons()
                }
                
            }) { (error) in
                UIAlertController.show(in: self, withTitle: "Error", message: error?.message, cancelButtonTitle: nil, otherButtonTitles: ["OK"], tap: { [weak self] (alert, index) in
                    guard let strongSelf = self else { return }
                    DB_MANAGER.removeFavoriteNews(strongSelf.article!)
                    strongSelf.navigationController?.popViewController(animated: true)
                })
            }
        }
        
        if let article = self.article {
            self.displayArticleInfo(article)
        }
        self.initNavigationButtons()
        self.initChangeFontPopup()
        DOWNLOAD_MANAGER.setDownloadDelegate(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        logInfo("FMArticleDetailsViewController deinit")
    }
    
    func initNavigationButtons (){
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH() / 2, height: 44))
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        self.catNameLabel = UILabel(frame: CGRect(x: 44, y: 0, width: leftView.width() - backButton.width(), height: 44))
        leftView.addSubview(self.catNameLabel)
        leftView.addSubview(backButton)
        backButton.setImage(#imageLiteral(resourceName: "ic_arrow_back"), for: .normal)
        backButton.addTarget(self, action: #selector(leftMenuButtonClicked), for: .touchUpInside)
        backButton.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0)
        catNameLabel.text = FMMenuSection.news.rawValue
        catNameLabel.font = UIFont.font(type: .Bold, size: 18)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftView)
        if let article = self.article {
            var favoriteButton : UIBarButtonItem? = nil
            if FMDBManager.sharedInstance.isFavoriteNews(article) {
                favoriteButton = UIBarButtonItem(image: UIImage(named:"ic_added_fav"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(favoriteButtonClicked))
            }
            else {
                favoriteButton = UIBarButtonItem(image: UIImage(named:"ic_fav"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(favoriteButtonClicked))
            }
            let sharingButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_share"), style: .plain, target: self, action: #selector(sharingButtonClicked))
            let fontButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_more_vert"), style: .plain, target: self, action: #selector(fontButtonClicked))
            
            self.navigationItem.rightBarButtonItems = [fontButton, sharingButton, favoriteButton!]
        }
    }
    
    func initChangeFontPopup() {
        
        let config = FTConfiguration.shared
        config.textColor = UIColor.black
        config.backgoundTintColor = UIColor.white
        config.borderColor = UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 1)
        config.menuWidth = 120
        config.menuSeparatorColor = UIColor.lightGray
        config.textAlignment = .left
        config.textFont = UIFont.font(type: .Bold, size: 13)
        config.menuRowHeight = 40
        config.cornerRadius = 3
        config.cellSelectionStyle = .gray
    }
    
    // MARK: -
    func displayArticleInfo(_ article: FMArticle) {
        
        self.article = article
        
        if let imageUrl = article.primaryImage {
            if let image = SDImageCache.shared().imageFromCache(forKey: imageUrl) {
                self.scaleAndDisplayMainImage(image)
            }
            else {
                SDWebImageDownloader.shared().downloadImage(with: URL(string: imageUrl), options: SDWebImageDownloaderOptions.useNSURLCache, progress: nil, completed: { (image, data, error, succeeded) in
                    SDWebImageManager.shared().saveImage(toCache: image, for: URL(string: imageUrl))
                    self.scaleAndDisplayMainImage(image)
                })
            }
        }
        else {
            self.imageViewHeightConstraint.constant = 0
        }
        
        self.dateLabel.text = article.detailDateText()
        self.titleLabel.text = article.title
//        self.introductionLabel.text = article.introduction
        self.preloadHTMLContent(article.introduction,textView: self.introductionTextView)
        self.preloadHTMLContent(article.content,textView: self.contentTextView)
        self.processHTMLImages(self.article?.content)

        UIView.animate(withDuration: 0.3) {
            self.loadingView.alpha = 0
        }
    }
    
    

    func prepareAuthor(_ authors: [FMAuthor]?) {
        guard let authors = authors else {
            return
        }
        if authors.count > 0 {
            self.authors = authors
//            self.authorsCollectionView.reloadData()

            
        }
    }
    
    
    func preparePhotos (_ photos: [String]?) {
        guard let photos = photos else {
            return
        }
        if photos.count > 0 {
            self.photos = photos
            
        }
    }
    
    func prepareRelatedPosts(_ posts: [FMArticle]?) {
        guard let posts = posts else {
            return
        }

        if posts.count > 0 {
            self.relatedPosts = posts
            


        }
    }
    
    func prepareFiles(_ files: [String]?) {
        guard let files = files else {
            return
        }
        if files.count > 0 {
            self.files = files
            
//            self.filesTableView.reloadData()
//            self.filesViewHeightConstraint.constant = CGFloat(files.count) * self.filesTableView.rowHeight + padding
        }
    }
    
    func setupSections(_ articleDetails: FMArticleDetails) {
        guard let layouts = articleDetails.layoutSettings else { return }
        var sectionHeight: CGFloat = 0
       
        
        for layout in layouts {
            if let postType = layout.postType  {
                switch postType {
                case .Files:
                    if (files.count == 0) {
                        break
                    }
                    let filesSection: FMFilesView = UIView.fromNib()
                    
                    sectionHeight = CGFloat(files.count) * 44 + 40
                    
                    filesSection.frame = CGRect(x: 0, y: totalSectionHeight, width: SCREEN_WIDTH(), height: sectionHeight)
                    
                    containerView.addSubview(filesSection)
                    totalSectionHeight += sectionHeight
                    
                    filesSection.tableView.delegate = self
                    filesSection.tableView.dataSource = self
                    filesSection.tableView.tag = 1000
                    filesSection.tableView.separatorStyle = .none
                    filesSection.tableView.register(UINib(nibName: "FMFilesTableViewCell", bundle: nil), forCellReuseIdentifier: "FMFilesTableViewCell")
                    filesSection.tableView.reloadData()
                    
                    
                case .RelatedNews:
                    if (relatedPosts.count == 0) {
                        break
                    }
                    let relatedNews: FMRelatedPostsView = UIView.fromNib()
                    sectionHeight = CGFloat(relatedPosts.count) * relatedNews.tableView.rowHeight + sectionNameHeight
                    relatedNews.frame = CGRect(x: 0, y: totalSectionHeight, width: SCREEN_WIDTH(), height: sectionHeight)
                    containerView.addSubview(relatedNews)
                    totalSectionHeight += sectionHeight
                    
                    relatedNews.tableView.delegate = self
                    relatedNews.tableView.dataSource = self
                    relatedNews.tableView.tag = 2000
                    relatedNews.tableView.separatorStyle = .none
                    
                    relatedNews.tableView.register(UINib(nibName: "FArticlesTableViewCell", bundle: nil), forCellReuseIdentifier: "smallArticlesCell")
                    relatedNews.tableView.reloadData()
                    
                    
                case .Gallery:
                    if (photos.count == 0) {
                        break
                    }
                    let gallerySection: FMGalleryView = UIView.fromNib()
                    let width = FMGalleryView.itemHeight
                    var rows = 0;
                    if photos.count < 3 {
                        rows = 1
                    }
                    else {
                        rows = (photos.count/3) + (photos.count % 3 == 0 ? 0: 1)
                    }
                    sectionHeight = CGFloat(rows) * (width) + CGFloat((rows - 1)) * minimumLineSpacing + 65
                    
                    gallerySection.frame = CGRect(x: 0, y: totalSectionHeight, width: SCREEN_WIDTH(), height: sectionHeight)
                    containerView.addSubview(gallerySection)
                    totalSectionHeight += sectionHeight
                    
                    gallerySection.collectionView.delegate = self
                    gallerySection.collectionView.dataSource = self
                    
                    
//                                        gallerySection.collectionView.register(FMGalleryCollectionViewCell.self, forCellWithReuseIdentifier: "FMGalleryCollectionViewCell")
                    gallerySection.collectionView.register(UINib(nibName: "FMGalleryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FMGalleryCollectionViewCell")
                    gallerySection.collectionView.tag = 2000
                    gallerySection.collectionView.reloadData()
                    
                case .Authors:
                    if (authors.count == 0) {
                        break
                    }
                    let authorsSection: FMAuthorsView = UIView.fromNib()
                    
//                    let width = (SCREEN_WIDTH() - 15)/2 - 10
                    let itemHeight = FMAuthorsView.itemHeight
                    var rows = 0
                    if authors.count <= 2 {
                        rows = 1
                    }
                    else {
                        rows = (authors.count/2) + (authors.count % 2 == 0 ? 0 : 1)
                    }
                   
                    sectionHeight = CGFloat(rows) * (itemHeight) + CGFloat((rows - 1)) * minimumLineSpacing + 50
                    
                    authorsSection.frame = CGRect(x: 0, y: totalSectionHeight, width: SCREEN_WIDTH(), height: sectionHeight)
                    
//                    bottomStackView.addArrangedSubview(authorsSection)
                    containerView.addSubview(authorsSection)
                    totalSectionHeight += sectionHeight
                    
                    authorsSection.collectionView.delegate = self
                    authorsSection.collectionView.dataSource = self
                    authorsSection.collectionView.tag = 1000
                    
//                    authorsSection.collectionView.register(FMAuthorsCollectionViewCell.self, forCellWithReuseIdentifier: "FMAuthorsCollectionViewCell")
                   authorsSection.collectionView.register(UINib(nibName: "FMAuthorsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FMAuthorsCollectionViewCell")
                    authorsSection.collectionView.reloadData()
                }
            }
            
        }
        
        
        stackviewHeightCnstr.constant = totalSectionHeight
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

    
    // MARK: -
    func scaleAndDisplayMainImage(_ image: UIImage?) {
        if let image = image {
            DispatchQueue.main.async {
                if image.size.width > self.articleImageView.width() {
                    let newImage = image.resize(scale: self.articleImageView.width()/image.size.width)
                    self.imageViewHeightConstraint.constant = newImage.size.height
                    self.articleImageView.image = image
                }
                else {
                    self.articleImageView.contentMode = .scaleAspectFit
                    self.articleImageView.image = image
                }

            }
        }
    }
    
    
    
    func preloadHTMLContent (_ html: String?, textView: UITextView) {
        if let content = html {
            do {
                let doc = try SwiftSoup.parse(content)
                let images = try doc.select("img")
                for imageEl in images {
                    try imageEl.remove()
                }
                
                self.loadHTMLContent(try doc.html(), textView: textView)
            }
            catch {}
        }
    }
    
    func loadHTMLContent (_ html: String?, textView: UITextView) {
        if let html = html {
            self.getContentAttribute(fromHTML: html, textView: textView) { (htmlAttr) in
                textView.attributedText = htmlAttr
                textView.sizeToFit()
                if textView === self.contentTextView {
                    
                    
                    self.contentViewHeightConstraint.constant = self.contentTextView.height()
                    UIView.animate(withDuration: 0.3) {
                        self.loadingView.alpha = 0
                    }
                }
                else if textView === self.introductionTextView {
                    self.introductionHeightCnstr.constant = self.introductionTextView.height()
                }
                
            }
        }
    }
    
    func processHTMLImages (_ html: String?) {
        if let content = html {
            do {
                var urls : [URL] = []
                let doc = try SwiftSoup.parse(content)
                let images = try doc.select("img")
                for imageEl in images {
                    let imageUrl = try imageEl.attr("src")
                    if let url = URL(string: imageUrl) {
                        urls.append(url)
                        let key = SDWebImageManager.shared().cacheKey(for: url)
                        if let localPath = SDImageCache.shared().defaultCachePath(forKey: key) {
                            try imageEl.removeAttr("src")
                            try imageEl.attr("src", "file://\(localPath)")
                        }
                    }
                }
                
                let html = try doc.html()
                
                if urls.count > 0 {
                    SDWebImagePrefetcher.shared().prefetchURLs(urls, progress: nil, completed: { (totalCount, skippedCount) in
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            self.loadHTMLContent(html, textView: self.contentTextView)
                        })
                    })
                }
            }
            catch {}
        }
    }
    
    // MARK: -
    @IBAction func fontButtonClicked(_ sender: Any, event: UIEvent) {
        FTPopOverMenu.showForEvent(event: event, with: fontSizeTitles, menuImageArray: [], done: { [weak self] (selectedIndex) -> () in
            guard let strongSelf = self else { return }
            guard selectedIndex < strongSelf.fontSizes.count else { return }
            
            let selectFontSize = strongSelf.fontSizes[selectedIndex].getFontSize()
            
            let fontSize = USER_DEFAULT_GET(key: .appFontSize)
            if let size = fontSize as? CGFloat {
                if size != selectFontSize {
                     USER_DEFAULT_SET(value: selectFontSize, key: .appFontSize)
                    strongSelf.formatContent(fontSize: selectFontSize)
                }
                
            }
            else {
                USER_DEFAULT_SET(value: selectFontSize, key: .appFontSize)
                strongSelf.formatContent(fontSize: selectFontSize)
            }
           
            
        }) {
            
        }
    }
    @IBAction func sharingButtonClicked(_ sender: Any) {
        guard let article = self.articleDetail else { return }
        let url = article.sharingUrl ?? ""
        let text = article.sharingText ?? ""
        let activityViewController = UIActivityViewController(activityItems: [text,url], applicationActivities: nil)
        activityViewController.completionWithItemsHandler = {(type, completed, items, error) in
//            print("Done")
            //UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).tintColor = .white
        }
        
        present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func favoriteButtonClicked(_ sender: Any) {
        if let article = self.article {
            if FMDBManager.sharedInstance.isFavoriteNews(article) {
                FMDBManager.sharedInstance.removeFavoriteNews(article)
            }
            else {
                FMDBManager.sharedInstance.addNewsToFavorite(article)
            }
            
            self.initNavigationButtons()
        }

    }
    
    @IBAction func outerLinkButtonClicked(_ sender: Any) {
    }
    @IBAction func authorButtonClicked(_ sender: Any) {
    }
    
    override func leftMenuButtonClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: -
    func getContentAttribute(fromHTML html: String, textView: UITextView, withCompletion completion: @escaping (NSMutableAttributedString) -> Void){
        DispatchQueue.global().async {
            var htmlAttr = NSMutableAttributedString()
            do {
                if let data = html.data(using: String.Encoding.unicode, allowLossyConversion: true) {
                    htmlAttr = try NSMutableAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                   NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
                    htmlAttr.fitImage(onWidth: textView.width())
                    
                    htmlAttr.applyAppFont(self.appFontSize)
                }
            }catch {}
            DispatchQueue.main.async {
                completion(htmlAttr)
            }
        }
    }
    
    func newsIdFromURL(_ url: String) -> Int?{
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == "id" })?.value?.toInt()
    }
}
extension FMArticleDetailsViewController : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if let newsId = self.newsIdFromURL(URL.absoluteString) {
            let articleDetailsVC = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMArticleDetailsViewController") as! FMArticleDetailsViewController
            articleDetailsVC.articleId = newsId
            self.navigationController?.pushViewController(articleDetailsVC, animated: true)
        }
        else {
            UIApplication.shared.openURL(URL)
        }
        return false
    }
}

extension FMArticleDetailsViewController : UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 0
//    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 2000 {
            return self.relatedPosts.count
        }

        return files.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 2000 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "smallArticlesCell", for: indexPath) as! FArticlesTableViewCell
            cell.setData(self.relatedPosts[indexPath.row], forThumbnail: true,isBottomLineHidden: indexPath.row == self.relatedPosts.count - 1)

            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FMFilesTableViewCell", for: indexPath) as! FMFilesTableViewCell
            cell.delegate = self
            if let article = self.article {
                cell.bindData(self.files[indexPath.row], newsId: article.id, isBottomlineHidden: indexPath.row == self.files.count - 1)
            }
            return cell
        }

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Connectivity.isNetworkAvailable {
            let articleDetailsVC = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMArticleDetailsViewController") as! FMArticleDetailsViewController
            articleDetailsVC.articleId = self.relatedPosts[indexPath.row].id
            self.navigationController?.pushViewController(articleDetailsVC, animated: true)
        }
        else {
            UIApplication.currentAppDelegate().showNoNetworkConnection()
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension FMArticleDetailsViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 2000 {
            return self.photos.count
        }
        return self.authors.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 2000 {
            
            return CGSize(width: FMGalleryView.itemWidth, height: FMGalleryView.itemHeight)
        }
        else {

            return CGSize(width: FMAuthorsView.itemWidth, height: FMAuthorsView.itemHeight)
        }


    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 2000  { // gallery
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FMGalleryCollectionViewCell", for: indexPath) as! FMGalleryCollectionViewCell
            cell.bindData(self.photos[indexPath.row])
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FMAuthorsCollectionViewCell", for: indexPath) as! FMAuthorsCollectionViewCell
            cell.bindData(self.authors[indexPath.row])

            return cell
        }

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 2000 {
            var photoObjs : [INSPhotoViewable] = []
            for url in self.photos {
                let photo = FMPhoto(imageURL: URL(string: url),thumbnailImageURL: URL(string: url))
                photoObjs.append(photo)
            }

            let cell = collectionView.cellForItem(at: indexPath) as! FMGalleryCollectionViewCell
            let currentPhoto = photoObjs[indexPath.row]
            let galleryPreview = INSPhotosViewController(photos: photoObjs, initialPhoto: currentPhoto, referenceView: cell)
            (galleryPreview.overlayView as! INSPhotosOverlayView).navigationItem.rightBarButtonItem = nil
            galleryPreview.referenceViewForPhotoWhenDismissingHandler = { photo in
                if let index = photoObjs.index(where: {$0 === photo}) {
                    let indexPath = NSIndexPath(item: index, section: 0)
                    return collectionView.cellForItem(at: indexPath as IndexPath) as? FMGalleryCollectionViewCell
                }
                return nil
            }
            present(galleryPreview, animated: true, completion: nil)
        }
        else {
            let author = self.authors[indexPath.row]

            guard let urlString = author.url, let url = URL(string: urlString) else { return }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(url)
            }
        }
    }
}

extension FMArticleDetailsViewController : FMDownloadManagerDelegate {
    func downloadManagerDidFinish(_ url: URL, newsId: Int) {
        var filesSectionView: UIView?
        for view in containerView.subviews {
            if view.isKind(of: FMFilesView.self) {
                filesSectionView = view
                break
            }
        }
        
        if let view = filesSectionView, let tableview = view.viewWithTag(1000),  let cell = self.cellForURL(url.absoluteString, newsId: newsId, tableView: (tableview as! UITableView)), let indexPath = (tableview as! UITableView).indexPath(for: cell) {
            (tableview as! UITableView).reloadRows(at: [indexPath], with: .fade)
        }
    }
    
    func downloadManagerDidUpdateProgress(_ url: URL, newsId: Int, progress: Float) {
        
        
        var filesSectionView: UIView?
        for view in containerView.subviews {
            if view.isKind(of: FMFilesView.self) {
                filesSectionView = view
                break
            }
        }
        
        if let view = filesSectionView, let tableview = view.viewWithTag(1000), let cell = self.cellForURL(url.absoluteString, newsId: newsId, tableView: (tableview as! UITableView)) {
            cell.updateDownloadProgress(progress)
        }
    }
    
    func downloadManagerDidFailure(_ url: URL, newsId: Int, error: Error?) {
        var filesSectionView: UIView?
        for view in containerView.subviews {
            if view.isKind(of: FMFilesView.self) {
                filesSectionView = view
                break
            }
        }
        
        if let view = filesSectionView, let tableview = view.viewWithTag(1000), let cell = self.cellForURL(url.absoluteString, newsId: newsId, tableView: (tableview as! UITableView)), let indexPath = (tableview as! UITableView).indexPath(for: cell) {
            (tableview as! UITableView).reloadRows(at: [indexPath], with: .fade)
        }
    }
    
    func cellForURL(_ url: String,  newsId: Int, tableView: UITableView) -> FMFilesTableViewCell?{
        if let article = self.article {
            if article.id == newsId {
                if let index = self.files.index(of: url) {
                    let indexPath = NSIndexPath(row: index, section: 0)
                    if let cell = tableView.cellForRow(at: indexPath as IndexPath) as? FMFilesTableViewCell {
                        return cell
                    }
                }
            }
        }
        
        return nil
    }
}

import QuickLook

extension FMArticleDetailsViewController : FMFilesTableViewCellDelegate {
    func filesTableViewCell(_ cell: FMFilesTableViewCell, didClickUtilityButton withData: (String, Int)) {
        
        let path = DOWNLOAD_MANAGER.localPath(forUrl: withData.0, newsId: withData.1)
        
        if FileManager.default.fileExists(atPath: path) {
            //preview the file
            self.previewPath = path
            let quickLookController = QLPreviewController()
            quickLookController.currentPreviewItemIndex = 0
            quickLookController.dataSource = self
            self.present(quickLookController, animated: true, completion: nil)
        }
        else { //dowload the file
            if !DOWNLOAD_MANAGER.isDownloading(withData.0) {
                DOWNLOAD_MANAGER.downloadFile(URL(string: withData.0)!, newsId: withData.1, destPath: path)
            }
        }
    }
}

extension FMArticleDetailsViewController :  QLPreviewControllerDataSource {
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return URL(fileURLWithPath: self.previewPath!) as QLPreviewItem
    }
}

class FMPhoto : INSPhoto {
    override func loadImageWithURL(_ url: URL?, completion: @escaping (UIImage?, Error?) -> ()) {
        if let url = url {
            let key = SDWebImageManager.shared().cacheKey(for: url)
            if let image = SDImageCache.shared().imageFromCache(forKey: key) {
                completion(image, nil)
            }
            else {
                SDWebImageDownloader.shared().downloadImage(with: url, options: SDWebImageDownloaderOptions.useNSURLCache, progress: nil, completed: { (image, data, error, succeeded) in
                    completion(image, error)
                })
            }
        }
    }
}

extension FMArticleDetailsViewController {
    fileprivate func formatContent(fontSize: CGFloat) {
        let attString = NSMutableAttributedString(attributedString: self.contentTextView.attributedText)
        attString.applyAppFont(fontSize)
        self.contentTextView.attributedText = attString
        self.contentTextView.sizeToFit()
        self.contentViewHeightConstraint.constant = self.contentTextView.height()
        self.titleLabel.font = self.titleLabel.font.withSize(fontSize + 4)
        self.dateLabel.font = self.dateLabel.font.withSize(fontSize + 3)
//        self.introductionLabel.font = self.introductionLabel.font.withSize(fontSize + 3)
        
        let introductionString = NSMutableAttributedString(attributedString: self.introductionTextView.attributedText)
        introductionString.applyAppFont(fontSize)
        self.introductionTextView.attributedText = introductionString
        self.introductionTextView.sizeToFit()
        self.introductionHeightCnstr.constant = self.introductionTextView.height()
        
    }
}
