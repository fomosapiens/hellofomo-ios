//
//  FMAuthorsCollectionViewCell.swift
//  Fomo
//
//  Created by Tuan Nguyen on 9/19/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class FMAuthorsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var authorNameLabel: UILabel!
    override func awakeFromNib() {
        self.authorImageView.clipsToBounds = true
        self.authorNameLabel.font = UIFont.font(type: .Bold, size: 14)
    }
    
    func bindData(_ author: FMAuthor) {
        self.authorNameLabel.text = author.name
        if let photo = author.photo {
            self.authorImageView.sd_setImage(with: URL(string: photo), placeholderImage: PLACEHOLDER_IMAGE)
        }
        else {
            self.authorImageView.image = PLACEHOLDER_IMAGE
        }
        
        
//        self.authorImageView.sd_setImage(with: URL(string: "http://fomo.us.elidev.info/uploads/1/469a3dff1b8a6ef94750e1a6d10245e1.jpg"), placeholderImage: PLACEHOLDER_IMAGE)
    }
}
