//
//  FMFilesView.swift
//  Fomo
//
//  Created by Tuan Nguyen on 9/19/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class FMFilesView: UIView {

    @IBOutlet weak var tableView: UITableView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
