//
//  FMCategoryViewController.swift
//  Fomo
//
//  Created by Khoa Bui on 7/4/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class FMCategoryViewController: UIViewController {

    static func initWithStoryboard() -> FMCategoryViewController{
        let viewController = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMCategoryViewController") as! FMCategoryViewController
        return viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
