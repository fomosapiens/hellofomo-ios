//
//  FMSettingsViewController.swift
//  Fomo
//
//  Created by Tuan Nguyen on 9/20/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class FMSettingsViewController: UIViewController {

    @IBOutlet weak var pushStatusSwitch: UISwitch!
    @IBOutlet weak var enableLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var enablePush = true
        
        if let enable = USER_DEFAULT_GET(key: .enablePushNotification) as? Bool {
            enablePush = enable
        }
        self.pushStatusSwitch.isOn = enablePush
        
        self.enableLabel.font = UIFont.font(type: .Bold, size: 17)
        self.titleLabel.font = UIFont.font(type: .Medium, size: 17)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func pushStatusSwitchValueChanged(_ sender: Any) {
        USER_DEFAULT_SET(value: pushStatusSwitch.isOn, key: .enablePushNotification)
        UIApplication.currentAppDelegate().enablePushNotification(pushStatusSwitch.isOn)
    }
   
    @IBAction func closeButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
