  //
//  FMSplashScreenViewController.swift
//  Fomo
//
//  Created by Tuan Nguyen on 8/1/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import SwiftDate
  
class FMSplashScreenViewController: UIViewController {

    @IBOutlet weak var splashImageView: UIImageView!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    @IBOutlet weak var retryLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    var tapGesture: UITapGestureRecognizer!
    var isLoading = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textView.isHidden = true
        
        self.tapGesture  = UITapGestureRecognizer(target: self, action: #selector(reloadData))

        self.reloadData()
    }

    deinit {
        logInfo("FMSplashScreenViewController deinit")
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func appLaunchImage() -> UIImage? {
        let allPngImageNames = Bundle.main.paths(forResourcesOfType: "png", inDirectory: nil)
        
        for imageName in allPngImageNames
        {
            guard imageName.contains("LaunchImage") else { continue }
            
            guard let image = UIImage(named: imageName) else { continue }
            
            // if the image has the same scale AND dimensions as the current device's screen...
            
            if (image.scale == UIScreen.main.scale) && (image.size.equalTo(UIScreen.main.bounds.size))
            {
                return image
            }
        }
        
        return nil
    }
    
    // MARK: -
    func reloadData () {
        if isLoading {
            return
        }
        
        DispatchQueue.main.async {
            
            self.view.removeGestureRecognizer(self.tapGesture)
            NotificationCenter.default.removeObserver(self)
            
            if Connectivity.isNetworkAvailable {
                self.prepareAppDataIfNeeded()
            }
                
            else {
                self.loadingActivity.isHidden = true
                self.view.addGestureRecognizer(self.tapGesture)
                NotificationCenter.default.addObserver(self, selector: #selector(self.handleNetworkReachable), name: NSNotification.Name(rawValue: "NetworkReachable"), object: nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    UIApplication.currentAppDelegate().showNoNetworkConnection()
                })
            }
        }
    }
    
    func handleNetworkReachable () {
        DispatchQueue.global().asyncAfter(deadline: .now() + 1) { 
            self.reloadData()
        }
    }
    // MARK: -
    func prepareAppDataIfNeeded () {
        
        self.loadingActivity.isHidden = false
        self.retryLabel.isHidden = true
        self.isLoading = true
        
        self.registerDevice { (error) in
            if error == nil {
                DB_MANAGER.getFavoriteNews()
                API_MANAGER.requestGetCategories(withCompletion: { (categories) in
                    if let categories = categories {
                        FMCategory.saveCategories(categories)
                        
                        let group = DispatchGroup()
                        group.enter()
                        API_MANAGER.requestGetAppFont(withCompletion: { (fontName) in
                        USER_DEFAULT_SET(value: fontName, key: .appFont)
                            group.leave()
                        })
                        
                        group.enter()
                        LAYOUT_MANAGER.createLayoutDashboard(layout: .Dashboard, completion: { (isSuccess) in
                            group.leave()
                        })
                        
                        group.enter()
                        LAYOUT_MANAGER.createLayoutDashboard(layout: .News, completion: { (isSuccess) in
                            group.leave()
                        })
                        
//                        group.enter()
//                        LAYOUT_MANAGER.createLayoutDashboard(layout: .Category, completion: { (isSuccess) in
//                            group.leave()
//                        })
                        
                        group.notify(queue: DispatchQueue.main) {
                            UIApplication.currentAppDelegate().createMenuView()
                            if let userInfo = UIApplication.currentAppDelegate().userInfo {
                                UIApplication.currentAppDelegate().handleReceivePushNotificationWithUserInfo(userInfo: userInfo)
                            }
                        }
                    }
                    else {
                        self.displayError()
                    }
                })
            }
            else {
                self.displayError()
            }
        }
    }
    
    func registerDevice(withCompletion completion:@escaping (FMResponse?) -> Void) {
        
        if self.isTokenExpired(){
            API_MANAGER.requestRegisterDevice(success: { (token) in
                completion(nil)
            }) { (error) in
                completion(error)
            }
        }
        else {
            completion(nil)
        }
    }
    
    func isTokenExpired () -> Bool {
        if USER_DEFAULT_GET(key: .userToken) == nil {
            return true
        }
        let date = USER_DEFAULT_GET(key: .tokenExpireDate) as? Date
        if let date = date {
            return date < Date()
        }
        
        return true
    }
    
    func displayError () {
        
        USER_DEFAULT_SET(value: nil, key: .userToken)
        
        self.isLoading = false
        // display error message
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.loadingActivity.isHidden = true
            self.retryLabel.isHidden = false
            self.view.addGestureRecognizer(self.tapGesture)
            
            UIAlertController.show(in: self, withTitle: "Error", message: "An error occurred. Please try again later.", cancelButtonTitle: nil, otherButtonTitles: ["OK"], tap: nil)
        })

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
