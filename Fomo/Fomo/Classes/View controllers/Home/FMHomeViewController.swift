//
//  FMHomeViewController.swift
//  Fomo
//
//  Created by Khoa Bui on 7/3/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class FMHomeViewController: BaseViewController {
    
    static func initWithStoryboard() -> FMHomeViewController{
        let viewController = MAIN_STORYBOARD().instantiateViewController(withIdentifier: "FMHomeViewController") as! FMHomeViewController
        return viewController
    }
    
    @IBOutlet weak var categoryScrollView: UIScrollView!
    @IBOutlet weak var articlesScrollView: UIScrollView!
    @IBOutlet weak var articlesContentView: UIView!
    @IBOutlet weak var articleHeightConstraints: NSLayoutConstraint!
    
    var articleVCArray:[FMArticlesViewController] = []
    var categoryVCArray:[FMCategoryView] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.setupConstraintLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    deinit {
        logInfo("FMHomeViewController deinit")
    }
}

//MARK:- Private method
extension FMHomeViewController {
    
    func displayCategoryAtIndex(_ index: Int) {
        self.articlesScrollView.setContentOffset(CGPoint(x: self.articleVCArray[index].view.frame.origin.x, y: 0), animated: true)
        self.highlightCategory(atIndex: index)
    }
    
    
    fileprivate func setupView() {
        self.title = FMMenuSection.news.rawValue
        self.showRightButton()
        self.showLeftButton()
        
        self.articlesScrollView.isDirectionalLockEnabled = true
        
        self.articleVCArray = []
        self.categoryVCArray = []
        for category in FMCategory.getCategories() {
            let vc =  FMArticlesViewController.initWithStoryboard()
            vc.category = category
            let catView = FMCategoryView.initWithCategory(category)
            catView.delegate = self
            
            articleVCArray.append(vc)
            categoryVCArray.append(catView)
        }
        
        self.addArticeViews()
        self.addCategoryViews()
    }

    func addCategoryViews() {
        var lastSubView: UIView?
        for subView in self.categoryVCArray {
            self.categoryScrollView.addSubview(subView)
            
            // Set autolay constraint
            subView.snp.makeConstraints {(make) in
                
                make.top.bottom.equalTo(self.categoryScrollView)
                make.width.equalTo(SCREEN_WIDTH()/2)
                if let lastSubView = lastSubView {
                    make.left.equalTo(lastSubView.snp.right)
                } else {
                    make.left.equalTo(self.categoryScrollView).offset(SCREEN_WIDTH()/4)
                }
                if subView == self.categoryVCArray.last {
                    make.right.equalTo(self.categoryScrollView).offset(-SCREEN_WIDTH()/4)
                }
                lastSubView = subView
            }
        }
        
        self.highlightCategory(atIndex: 0)
    }
    
    func addArticeViews() {
        
        var lastVCInArticles: UIViewController?
        for childVC in self.articleVCArray {
            self.addChildViewController(childVC)
            self.articlesContentView.addSubview(childVC.view)
            childVC.didMove(toParentViewController: self)
            
            // Set autolay constraint
            childVC.view.snp.makeConstraints {(make) in
                
                make.top.bottom.equalTo(self.articlesContentView)
                make.width.equalTo(SCREEN_WIDTH())
                if let lastVCInArticles = lastVCInArticles {
                    make.left.equalTo(lastVCInArticles.view.snp.right)
                } else {
                    make.left.equalTo(self.articlesContentView)
                }
                if childVC == self.articleVCArray.last {
                    make.right.equalTo(self.articlesContentView)
                }
                lastVCInArticles = childVC
            }
        }
    }
    
    fileprivate func highlightCategory(atIndex index:Int) {
        
        if self.categoryVCArray.count == self.articleVCArray.count {
            for (indexPage, categoryVC) in self.categoryVCArray.enumerated() {
                if indexPage == index {
                    categoryVC.categoryButton.setActiveColor()
                }
                else {
                    categoryVC.categoryButton.setDeactiveColor()
                }
            }
        }
    }
    
    
    fileprivate func setupConstraintLayout() {
        self.articleHeightConstraints.constant = SCREEN_HEIGHT()-50
    }
    
}

extension FMHomeViewController : FMCategoryViewDelegate {
    func categoryView(_ view: FMCategoryView, didSelectCategory category: FMCategory?) {
        if let index = self.categoryVCArray.index(of: view) {
            self.articlesScrollView.setContentOffset(CGPoint(x: self.articleVCArray[index].view.frame.origin.x, y: 0), animated: true)
            self.highlightCategory(atIndex: index)
        }
    }
}
//MARK:- Public method
extension FMHomeViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.articlesScrollView {
            let newOffsetX = (self.categoryScrollView.contentSize.width - SCREEN_WIDTH()/2) * scrollView.contentOffset.x / scrollView.contentSize.width
            self.categoryScrollView.setContentOffset(CGPoint(x: newOffsetX, y: 0), animated: false)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.articlesScrollView {
            let indexPage = Int(scrollView.contentOffset.x / scrollView.bounds.size.width)
            self.highlightCategory(atIndex: indexPage)
        }
    }
}
