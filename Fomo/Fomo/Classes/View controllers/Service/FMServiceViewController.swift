//
//  FMServiceViewController.swift
//  Fomo
//
//  Created by Tuan Nguyen on 8/8/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class FMServiceViewController: UIViewController {

    var serviceType: FMServiceType!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var noNetworkLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contentTextView.isEditable = false
        
        if Connectivity.isNetworkAvailable {
            self.noNetworkLabel.isHidden = true
            APIManager.sharedInstance.requestGetService(type: self.serviceType) { (content) in
                if let content = content {
                    do {
                        let htmlAttr = try NSMutableAttributedString(data: content.data(using: String.Encoding.unicode)!, options: [NSDocumentTypeDocumentAttribute : NSHTMLTextDocumentType], documentAttributes: nil)
                        htmlAttr.fitImage(onWidth: self.contentTextView.width())
                        htmlAttr.applyAppFont()
                        self.contentTextView.attributedText = htmlAttr
                        
                    }catch {}
                }
                self.loadingActivityIndicator.isHidden = true
            }
        }
        else {
            self.noNetworkLabel.isHidden = false
            self.loadingActivityIndicator.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
