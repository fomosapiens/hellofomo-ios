//
//  Macros.swift
//  SingleMom
//
//  Created by Khoa Bui on 1/5/17.
//  Copyright © 2017 SingleMom. All rights reserved.
//

import Foundation
import UIKit
import KeychainAccess

typealias CallBackEvent = (_ object: AnyObject?) -> Void

var mainTintColor = UIColor.white
var mainBackgroundColor = UIColor(red: 55/255, green: 71/255, blue: 79/255, alpha: 1)
var mainNavigationColor = UIColor(red: 38/255, green: 50/255, blue: 56/255, alpha: 1)

let KEYCHAIN_SERVICE = "org.fomosapiens.rgc"
let KEYCHAIN_DEVICE_ID = "device_id"

let minimumLineSpacing: CGFloat = 15

let minimumInteritemSpacing: CGFloat = 15

enum FMLanguage: String {
    case de = "de"
    case en = "en"
}

enum FLayoutType {
    case Dashboard
    case News
    case Category
}

enum FMFontName: String {
    case TimesNewRoman = "Times New Roman"
    case Titilium = "Titilium"
    case WorkSans = "Work Sans"
    case Roboto = "Roboto"
    case OpenSans = "Open Sans"
    case Helvetica = "Helvetica"
    case Courier = "Courier"
    case CourierBold = "Courier-Bold"
    case LatoBold = "Lato-Bold"
    case LatoLight = "Lato-Light"
    case Lato = "Lato"
    case Arial = "Arial"
    
    func getFontFamily() -> String {
        switch self {
        case .TimesNewRoman:
            return "TimesNewRomanPSMT"
        case .Titilium:
            return "TitilliumWeb"
        case .WorkSans:
            return "WorkSans"
        case .OpenSans:
            return "OpenSans"
        case .Helvetica:
            return "HelveticaNeue"
        case .Courier, .CourierBold:
            return "Courier"
        case .Lato, .LatoBold, .LatoLight:
            return "Lato"
        case .Arial:
            return "ArialHebrew"
        default:
            return "Roboto"
        }
    }
}
enum FMFontTypes: String {
    case Bold = "Bold"
    case SemiBold = "SemiBold"
    case Regular = "Regular"
    case Italic = "Italic"
    case Light = "Light"
    case Thin = "Thin"
    case Medium = "Medium"
}

enum FMFontSizes: String {
    case normal = "Schriftgröße klein"
    case big = "Schriftgröße normal"
    case extraBig = "Schriftgröße groß"
    
    func getFontSize() -> CGFloat {
        switch self {
        case .normal:
            return 18
        case .big:
            return 20
        case .extraBig:
            return 22
        }
    }
}

enum FMServiceType : Int {
    case contact
    case privacy
    case impressum
    case agb
    
    var stringValue : String {
        get {
            if self == .contact {
                return "Kontakt"
            }
            else if self == .privacy {
                return "Datenschutz"
            }
            else if self == .impressum {
                return "Impressum"
            }
            
            return "AGB"
        }
    }
}

enum FMMenuSection : String {
    case dashboard = "STARTSEITE"
    case news = "NACHRICHTEN"
    case service = "SERVICE"
    case settings = "EINSTELLUNGEN"
}


enum FMUserDefaultKeys: String {
    case deviceToken = "deviceToken"
    case currentUser = "currentUser"
    case userToken = "userToken"
    case appFont = "appFont"
    case categories = "categories"
    case tokenExpireDate = "tokenExpireDate"
    case favorites = "favorites"
    case enablePushNotification = "enablePushNotification"
    case appFontSize = "appFontSize"
    
}

var PATH_OF_DOCUMENT : String {
    get {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
}


func LOCALIZE(key: String) -> String {
    return NSLocalizedString(key, comment: "");
}

func USER_DEFAULT_SET(value: Any?,key: FMUserDefaultKeys) {
    UserDefaults.standard.set(value, forKey: key.rawValue)
}

func USER_DEFAULT_SYNC() {
    UserDefaults.standard.synchronize();
}

func USER_DEFAULT_GET(key: FMUserDefaultKeys) -> Any? {
    return UserDefaults.standard.object(forKey: key.rawValue)
}

var DEVICE_ID : String {
    get {
        let keychain = Keychain(service: KEYCHAIN_SERVICE)
        
        var deviceId = keychain[KEYCHAIN_DEVICE_ID]
        if deviceId == nil {
            var uuidRef:        CFUUID?
            var uuidStringRef:  CFString?
            
            uuidRef         = CFUUIDCreate(kCFAllocatorDefault)
            uuidStringRef   = CFUUIDCreateString(kCFAllocatorDefault, uuidRef)
            
            if (uuidRef != nil) {
                uuidRef = nil
            }
            
            if (uuidStringRef != nil) {
                deviceId = uuidStringRef! as String
            }
            
            keychain[KEYCHAIN_DEVICE_ID] = deviceId
        }
        
        return deviceId!
    }
}

//======================= SPECIFIED DEVICE ====================
func IS_IPAD() -> Bool{
    if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
        return true
    }
    return false
}

func IS_IPHONE() -> Bool{
    if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone {
        return true
    }
    return false
}

func IS_IPHONE4() -> Bool{
    if IS_IPHONE() && UIScreen.main.bounds.size.height == 480 {
        return true
    }
    return false
}

func IS_IPHONE5() -> Bool{
    if IS_IPHONE() && UIScreen.main.bounds.size.height == 568 {
        return true
    }
    return false
}

func IS_IPHONE6() -> Bool{
    if IS_IPHONE() && UIScreen.main.bounds.size.height == 667 {
        return true
    }
    return false
}

func IS_IPHONEPLUS() -> Bool{
    if IS_IPHONE() && UIScreen.main.bounds.size.height == 736 {
        return true
    }
    return false
}

//========================== SCREEN SIZE ==========================

func SCREEN_WIDTH() -> CGFloat{
    return UIScreen.main.bounds.size.width
}

func SCREEN_HEIGHT() -> CGFloat{
    return UIScreen.main.bounds.size.height
}

var SCREEN_SIZE : CGSize {
    get {
        return UIScreen.main.bounds.size
    }
}

func MAIN_STORYBOARD() -> UIStoryboard {
    return UIStoryboard(name: "Main", bundle: nil)
}

// COLOR
let ML_BLUE_COLOR = "00A559"
let ML_GRAY_COLOR = "D3D3D3"
var DIS_USER_ID : String?

//============================= COLOR =============================

func colorWithRGB(kRed: CGFloat, kGreen: CGFloat, kBlue: CGFloat, kAlpha: CGFloat) -> UIColor{
    return UIColor(red: kRed/255.0, green: kGreen/255.0, blue: kBlue/255.0, alpha: kAlpha)
}

//============================= IMAGE =============================

var PLACEHOLDER_IMAGE : UIImage? {
    get {
        return UIImage(named: "placeholder")
    }
}

func LOCALIZED_STRING(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

let loadFeedDataQueue = DispatchQueue(label: "com.vaster.dispatchgroup", qos: .userInteractive, attributes: .concurrent)
