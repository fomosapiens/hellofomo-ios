//
//  FMDBManager.swift
//  Fomo
//
//  Created by Tuan Nguyen on 9/18/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

let DB_MANAGER = FMDBManager.sharedInstance

class FMDBManager: NSObject {
    
    var favoriteNews : [FMArticle] = []
    var news : [String : [FMArticle]] = [:]
    
    static let sharedInstance = FMDBManager ()
    
    func cacheNews(_ news: [FMArticle] , forCategory category: String) {
        var result : [FMArticle] = []
        if let cachedNews = self.getNews(forCategory: category) {
            result = cachedNews
        }
        
        result.append(contentsOf: news)
        self.news[category] = result
    }
    
    func getNews(forCategory category: String) -> [FMArticle]? {
        return self.news[category]
    }
    
    func removeNews(forCategory category: String) {
        self.news.removeValue(forKey: category)
    }
    
    func isFavoriteNews(_ news : FMArticle) -> Bool{
        for article in favoriteNews {
            if article.id == news.id {
                return true
            }
        }
        
        return false
    }
    func removeFavoriteNews(_ news: FMArticle) {
        for (_,article) in self.favoriteNews.enumerated() {
            if article.id == news.id {
                self.favoriteNews.remove(article)
                break
            }
        }
        
        
        for (_,article) in favoriteNews.enumerated() {
            if article.id == news.id {
                favoriteNews.remove(article)
                break
            }
        }
        
        //try to add again
        
        var result : [Data] = []
        for article in favoriteNews {
            let data = NSKeyedArchiver.archivedData(withRootObject: article)
            result.append(data)
        }
        
        USER_DEFAULT_SET(value: result, key: .favorites)
    }
    
    func addNewsToFavorite(_ news: FMArticle) {
        var postData = USER_DEFAULT_GET(key: .favorites) as? [Data]
        if postData == nil {
            postData = []
        }
        
        let data = NSKeyedArchiver.archivedData(withRootObject: news)
        postData?.append(data)
        
        USER_DEFAULT_SET(value: postData, key: .favorites)
        
        self.favoriteNews.append(news)
    }
    
    func getFavoriteNews() {
        
        
        if let favorites = USER_DEFAULT_GET(key: .favorites) as? [Data] {
            var result : [FMArticle] = []
            let group = DispatchGroup()
            for data in favorites {
                let art = NSKeyedUnarchiver.unarchiveObject(with: data) as! FMArticle
                group.enter()
                API_MANAGER.requestGetNewsDetail(id: art.id, success: { (article) in
                    group.leave()
                    result.append(art)
                }) { (error) in
                    group.leave()
                }
                
            }
            
            group.notify(queue: DispatchQueue.main) {
                self.favoriteNews = result
                
                let data = self.archivedData(of: result)
                
                //update favorites
                USER_DEFAULT_SET(value: data, key: .favorites)
                
            }
            
        }
    }
    
    func archivedData(of articles: [FMArticle]) -> [Data] {
        var articlesData: [Data] = []
        for article in articles {
            let data = NSKeyedArchiver.archivedData(withRootObject: article)
            articlesData.append(data)
        }
        
        return articlesData
    }
    
    
}
