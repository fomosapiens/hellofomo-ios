//
//  Connectivity.swift
//  Fomo
//
//  Created by Tuan Nguyen on 8/18/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import Alamofire

class Connectivity {
    class var isNetworkAvailable : Bool {
        return UIApplication.currentAppDelegate().isNetworkAvailable()
    }
}
