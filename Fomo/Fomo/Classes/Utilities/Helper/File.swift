//
//  File.swift
//  Fomo
//
//  Created by Quan Quach on 9/11/18.
//  Copyright © 2018 Elinext. All rights reserved.
//

import Foundation

extension UIView {
    class func fromNib<T: UIView>() -> T {
    
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
