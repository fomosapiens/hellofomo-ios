//
//  FMDownloadManager.swift
//  Fomo
//
//  Created by Tuan Nguyen on 9/20/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import Alamofire

protocol FMDownloadManagerDelegate : class {
    func downloadManagerDidUpdateProgress(_ url: URL, newsId: Int, progress: Float)
    func downloadManagerDidFailure(_ url: URL, newsId: Int, error: Error?)
    func downloadManagerDidFinish(_ url: URL, newsId: Int)
}

let DOWNLOAD_MANAGER = FMDownloadManager.sharedInstance
class FMDownloadManager: NSObject {
    
    static let sharedInstance = FMDownloadManager()
    
    weak var delegate : FMDownloadManagerDelegate?
    var downloadingURLs : [String] = []
    var downloadingProgress : [String:Float] = [:]
    
    func setDownloadDelegate(_ delegate: FMDownloadManagerDelegate) {
        self.delegate = delegate
    }
    
    func isDownloading(_ url: String) -> Bool {
        return self.downloadingURLs.contains(url)

    }
    
    func isDownloaded(_ url: String, newsId: Int) -> Bool {
        let path = self.localPath(forUrl: url, newsId: newsId)
        return FileManager.default.fileExists(atPath: path)
    }
    
    func downloadFile(_ url: URL, newsId: Int, destPath path: String) {
        
        self.downloadingURLs.append(url.absoluteString)

        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (URL(fileURLWithPath: path), [.removePreviousFile, .createIntermediateDirectories])
        }

        
        Alamofire.download(
            url,
            method: .get,
            parameters: nil,
            encoding: JSONEncoding.default,
            headers: nil,
            to: destination).downloadProgress(closure: { (progress) in
                
                let percentage =  Float(progress.completedUnitCount/progress.totalUnitCount)
                self.downloadingProgress[url.absoluteString] = percentage
                self.delegate?.downloadManagerDidUpdateProgress(url, newsId: newsId, progress: percentage)
                
            }).response(completionHandler: { (response) in
                
                self.downloadingURLs.remove(url.absoluteString)
                
                if (response.error != nil) {
                    self.delegate?.downloadManagerDidFailure(url, newsId: newsId, error: response.error)
                }
                else {
                    self.delegate?.downloadManagerDidFinish(url, newsId: newsId)
                }
            })
    }
    
    func localPath(forUrl url: String, newsId: Int) -> String{
        let name = (url as NSString).lastPathComponent
        let path = (PATH_OF_DOCUMENT as NSString).appendingPathComponent(String(newsId)) as NSString
        return path.appendingPathComponent(name)
    }
}
