//
//  VTLogging.swift
//  Vaster
//
//  Created by Tuan Nguyen on 4/18/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import CocoaLumberjack

class FMLogging: NSObject {
    static func setupLogging() {
        
        DDLog.add(DDTTYLogger.sharedInstance)
        
        #if DEBUG
            defaultDebugLevel = .all
        #else
            defaultDebugLevel = .warning
        #endif
    }
}

//log info
public func logInfo(_ message: String) {
    DDLogInfo(message)
}

//logDebug
public func logDebug(_ message: String) {
    DDLogDebug(message)
}

//logVerbose
public func logVerbose(_ message: String) {
    DDLogVerbose(message)
}

//logWarn
public func logWarn(_ message: String) {
    DDLogWarn(message)
}

//logError
public func logError(_ message: String) {
    DDLogError(message)
}
