//
//  BaseExtensions.swift
//  Driver-iOS
//
//  Created by Khoa Bui on 5/4/17.
//  Copyright © 2017 com.MaiLinh.iOS. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import SnapKit


extension UIFont {
    static func font(type: FMFontTypes, size: CGFloat) -> UIFont {
        if let fontName = USER_DEFAULT_GET(key: .appFont) as? String {
            let fontFamily = FMFontName(rawValue: fontName)
            
            guard let fontName = fontFamily else {
                return UIFont(name: "Roboto-\(type.rawValue)", size: size)!
            }
            
            let fullName = fontName.getFontFamily() + "-" + type.rawValue
            
            if let font = UIFont(name: fullName, size: size) {
                return font
            }
        }
        return UIFont(name: "Roboto-\(type.rawValue)", size: size)!
        
    }
}
// MARK: - UIApplication
extension UIApplication {
    
    static func currentAppDelegate() -> AppDelegate {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate
    }
    
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        
        if let slide = viewController as? SlideMenuController {
            return topViewController(slide.mainViewController)
        }
        return viewController
    }

}

extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(self.rootViewController)
    }
    
    public static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(nc.visibleViewController)
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(tc.selectedViewController)
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(pvc)
            } else {
                return vc
            }
        }
    }
}

//MARK:- UIColor

extension UIColor {
    
    static func colorWithHexString (_ hex:String) -> UIColor {
        var cString = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    class func blueLight() -> UIColor {
        return UIColor.colorWithHexString("#007AFF")
    }
}

// MARK: - UIViewController
extension UIViewController {
    
//    func add(childViewController childVC: UIViewController, inView view: UIView, withFrame frame: CGRect, completion: (() -> Void)? = nil) {
//        childVC.view.frame = frame
//        childVC.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        self.addChildViewController(childVC)
//        UIView.transition(with: view, duration: 0.2, options: .transitionCrossDissolve, animations: {
//            view.addSubview(childVC.view)
//        }, completion: { (_) in
//            completion?()
//        })
//        childVC.didMove(toParentViewController: self)
//    }
    
//    func addChild(viewController childVCArray: [UIViewController], inView view:UIView) {
//        for (index, childVC) in childVCArray.enumerated() {
//            self.addChildViewController(childVC)
//            
////            childVC.view.snp.makeConstraints { (make) in
////                //make.top.left.bottom.right.equalTo(view)
////                make.top.bottom.equalTo(view)
////                if index == 0 {
////                    make.left.equalTo(view)
////                    make.width = SCREEN_WIDTH()
////                } else {
////                    make.left.equalTo(view).offset(<#T##amount: ConstraintOffsetTarget##ConstraintOffsetTarget#>)
////                }
////            }
//            let frame = CGRect(x: SCREEN_WIDTH() * CGFloat(index), y: 0, width: SCREEN_WIDTH(), height: SCREEN_HEIGHT() - 50)
//            childVC.view.frame = frame
//            view.addSubview(childVC.view)
//            childVC.didMove(toParentVicurrentoller: self)
//            var currentFrameOfParentView = view.frame
//            currentFrameOfParentView.size.width += SCREEN_WIDTH()
//            view.frame = currentFrameOfParentView
//        }
//    }
    
    func remove(chileViewController childVC: UIViewController?) {
        childVC?.willMove(toParentViewController: nil)
        childVC?.view.removeFromSuperview()
        childVC?.removeFromParentViewController()
    }
    
    func appDelegate() -> AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    func showAlertWithTitle(_ title: String = "Warning", message mess: String,
                            okButton ok: String? = "Close", okHandler: ((UIAlertAction) -> Void)? = nil,
                            closeButton close: String? = nil, closeHandler: ((UIAlertAction) -> Void)? = nil,
                            completionHanlder completion: (()-> Void)? = nil) -> Void {
        let alertVC = UIAlertController(title: title, message: mess, preferredStyle: .alert)
        
        if let ok = ok {
            let okAction = UIAlertAction(title: ok, style: .cancel, handler: okHandler)
            alertVC.addAction(okAction)
        }
        
        if let close = close{
            let closeAction = UIAlertAction(title: close, style: .default, handler: closeHandler)
            alertVC.addAction(closeAction)
        }
        
        if close == nil && ok == nil{
            let defaultAction = UIAlertAction(title: "Close", style: .default, handler: closeHandler)
            alertVC.addAction(defaultAction)
        }
        
        self.present(alertVC, animated: true, completion: completion)
    }
    
    var firstPresentingViewController : UIViewController {
        get {
            
            var presentingController: UIViewController? = self
            while (presentingController?.presentingViewController != nil){
                presentingController = presentingController?.presentingViewController
            }
            
            return presentingController!
        }
    }
    
    
}
extension NSMutableAttributedString {
    func fitImage(onWidth width: CGFloat) {
        self.enumerateAttribute(NSAttachmentAttributeName, in: NSMakeRange(0, self.length), options: NSAttributedString.EnumerationOptions.init(rawValue: 0), using: { (value, range, stop) in
            if let attachement = value as? NSTextAttachment {
                let image = attachement.image(forBounds: attachement.bounds, textContainer: NSTextContainer(), characterIndex: range.location)
                if let image = image {
                    if image.size.width > width {
                        let newImage = image.resize(scale: width/image.size.width)
                        let newAttribut = NSTextAttachment()
                        newAttribut.image = newImage
                        self.addAttribute(NSAttachmentAttributeName, value: newAttribut, range: range)
                    }
                }
            }
        })
    }
    func applyAppFont (_ fontSize: CGFloat = 14) {
        // Enumerate through all the font ranges
        self.enumerateAttribute(NSFontAttributeName, in: NSMakeRange(0, self.length), options: []) { value, range, stop in
            guard let currentFont = value as? UIFont, let font = UIFont(name: currentFont.fontName, size: fontSize) else {
                return
            }
            
            // An NSFontDescriptor describes the attributes of a font: family name, face name, point size, etc.
            // Here we describe the replacement font as coming from the "Hoefler Text" family
//            let fontDescriptor = currentFont.fontDescriptor.addingAttributes([UIFontDescriptorFamilyAttribute: USER_DEFAULT_GET(key: .appFont)])
//            
//            // Ask the OS for an actual font that most closely matches the description above
//            if let newFontDescriptor = fontDescriptor.matchingFontDescriptors(withMandatoryKeys: [UIFontDescriptorFamilyAttribute]).first {
//                let newFont = UIFont(descriptor: newFontDescriptor, size: currentFont.pointSize + 5)
//                self.addAttributes([NSFontAttributeName: newFont], range: range)
//            }
            
            self.addAttributes([NSFontAttributeName: font], range: range)
        }
    }
}

extension UIImageView {
    func displayImage(_ url: String?, placeholderImage: UIImage?) {
        if let url = url{
            self.sd_setImage(with: URL(string: url), placeholderImage: placeholderImage)
        }
        else {
            self.image = placeholderImage
        }
    }
}

extension UIImage {
    func resize(scale: CGFloat) -> UIImage {
        let newSize = CGSize(width: self.size.width*scale, height: self.size.height*scale)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions( newSize, false, 0 )
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

//MARK:- UIImage
extension UIImage {
    func getPixelColor(_ pos: CGPoint) -> UIColor {
        
        let pixelData = self.cgImage?.dataProvider?.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        
        let pixelInfo: Int = ((Int(self.size.width) * Int(pos.y)) + Int(pos.x)) * 4
        
        let r = CGFloat(data[pixelInfo]) / CGFloat(255.0)
        let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
        let b = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
        let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
}

//MARK:- UIButton
extension UIButton {
    func rotateButton(animationDuration timeInterVal :CFTimeInterval, inSecond second:CFTimeInterval) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = timeInterVal
        let repeatCount = Float(second) / Float(rotateAnimation.duration)
        rotateAnimation.repeatCount = repeatCount
        self.layer.add(rotateAnimation, forKey: nil)
    }
    
    func stopButtonAnimation() {
        self.layer.removeAllAnimations()
    }
}

extension UIActivityIndicatorView {
    func beginAnimation(isStop: Bool = false) {
        if isStop {
            self.stopAnimating()
            self.isHidden = true
        } else {
            self.startAnimating()
            self.isHidden = false
        }
    }
}

// MARK: - String
extension String {
    func removeNonNumber() -> String {
        let correctCharacters: Set<Character> = Set("0123456789".characters)
        return String(self.characters.filter{correctCharacters.contains($0)})
    }
    
    func toInt() -> Int {
        let str = self.removeNonNumber()
        return Int(str) ?? 0
    }
    
    func toDouble() -> Double {
        let str = self.removeNonNumber()
        return Double(str) ?? 0
    }
    
    func moneyFormat() -> String {
        var str = self
        var count = str.characters.count
        while count > 3 {
            str.insert(".", at: str.index(str.startIndex, offsetBy: count-3))
            count -= 3
        }
        return "\(str) đ"
    }
}

extension String {
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.characters.count
    }
    
    func width(font: UIFont) -> CGFloat {
        let fontAttributes = [NSFontAttributeName: font]
        return (self as NSString).size(attributes: fontAttributes).width
    }
    
    func isValidEmailAddress() -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0 {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func phoneFormat() -> String {
        if self.characters.count >= 7 {
            var str = self
            str.insert(" ", at: str.index(str.startIndex, offsetBy: 3))
            str.insert(" ", at: str.index(str.startIndex, offsetBy: 7))
            return str
        } else {
            return self
        }
    }
    
    var parseJSONString: AnyObject?
    {
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if let jsonData = data
        {
            // Will return an object or nil if JSON decoding fails
            do
            {
                let message = try JSONSerialization.jsonObject(with: jsonData, options:.mutableContainers)
                if let jsonResult = message as? NSMutableArray {
                    return jsonResult //Will return the json array output
                } else if let jsonResult = message as? NSMutableDictionary {
                    return jsonResult //Will return the json dictionary output
                } else {
                    return nil
                }
            }
            catch let error as NSError
            {
                print("An error occurred: \(error)")
                return nil
            }
        }
        else
        {
            // Lossless conversion of the string was not possible
            return nil
        }
    }
}

// MARK: - Date
extension Date {
    var month: Int {
        return Calendar.current.component(.month, from: self)
    }
    
    var year: Int {
        return Calendar.current.component(.year, from: self)
    }
    
    var day: Int {
        return Calendar.current.component(.day, from: self)
    }
    
    var hour: Int {
        return Calendar.current.component(.hour, from: self)
    }
    
    var minute: Int {
        return Calendar.current.component(.minute, from: self)
    }
    
    func convertDate(toDateFormat format:String) -> String {
        
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: "en_US_POSIX")
        
        return formatter.string(from: self)
    }
}

extension Double {
    func currencyString() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "vi_VN")
//        return "\(formatter.string(from: ((self/1000).rounded() * 1000 as NSNumber))!)"
        return formatter.string(from: self as NSNumber)!
    }
    
    func toHoursString() -> String {
        let hours: Int = Int(self)/3600
        let minutes: Int = (Int(self)%3600)/60
        
        return "\(hours):\(minutes)"
    }
    
    func toMinute() -> Int {
        return Int((self/60).roundTo(places: 0))
    }
    
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

// MARK: - TimeInterval
extension TimeInterval {
    func toDate() -> Date {
        return Date(timeIntervalSince1970: self)
    }
    
    func toHourMinute() -> String {
        return "\(self.toDate().convertDate(toDateFormat: "HH:mm"))"
    }
    
    func toDate(withFormat strFormat: String = "MM/dd/yyyy, HH:mm") -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = strFormat
        
        return formatter.string(from: self.toDate())
    }
}

// MARK: - CLLocationCoordinate2D
extension CLLocationCoordinate2D {
    func distance(toCoordinate endCoor: CLLocationCoordinate2D) -> CLLocationDistance {
        let from = CLLocation(latitude: self.latitude, longitude: self.longitude)
        let to = CLLocation(latitude: endCoor.latitude, longitude: endCoor.longitude)
        
        return from.distance(from: to)
    }
}

//MARK: - Array

extension Array where Element:Equatable {
    public mutating func remove(_ item:Element ) {
        var index = 0
        while index < self.count {
            if self[index] == item {
                self.remove(at: index)
            } else {
                index += 1
            }
        }
    }
    
    public func array( removing item:Element ) -> [Element] {
        var result = self
        result.remove( item )
        return result
    }
}

//MARK: - UiColor
extension UIColor {
    
    convenience init(hex: String) {
        self.init(hex: hex, alpha:1)
    }
    
    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = hex.substring(1)
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.length) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            // TODO:ERROR
            break;
        }
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
    }
}

//MARK: - UIView
extension UIView {
    func origin() -> CGPoint {
        return self.frame.origin;
    }
    
    func setOrigin(newOrigin:CGPoint) {
        var newFrame = self.frame;
        newFrame.origin = newOrigin;
        self.frame = newFrame;
    }
    
    func size() -> CGSize {
        return self.frame.size;
    }
    
    func setSize(newSize: CGSize) {
        var newFrame = self.frame;
        newFrame.size = newSize;
        self.frame = newFrame;
    }
    
    func x() -> CGFloat {
        return self.frame.origin.x;
    }
    
    func setX(newX:CGFloat) {
        var newFrame = self.frame;
        newFrame.origin.x = newX;
        self.frame = newFrame;
    }
    
    func y() -> CGFloat {
        return self.frame.origin.y;
    }
    
    func setY(newY: CGFloat) {
        var newFrame = self.frame;
        newFrame.origin.y = newY;
        self.frame = newFrame;
    }
    
    func height() -> CGFloat {
        return self.frame.size.height;
    }
    
    func setHeight(newHeight: CGFloat) {
        var newFrame = self.frame;
        newFrame.size.height = newHeight;
        self.frame = newFrame;
    }
    
    func width() -> CGFloat {
        return self.frame.size.width;
    }
    
    func setWidth(newWidth: CGFloat) {
        var newFrame = self.frame;
        newFrame.size.width = newWidth;
        self.frame = newFrame;
    }
    
    //
    func imageData () -> Data {
        UIGraphicsBeginImageContextWithOptions(self.layer.frame.size, false, 2)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let viewImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        let data = UIImagePNGRepresentation(viewImage)
        return data!
    }
    //TODO: To be deleted
    /// Temporary function to animatedly bring a subview to front
    func animateView(toFront subView: UIView) {
        subView.alpha = 0;
        self.bringSubview(toFront: subView);
        UIView.animate(withDuration: 0.2) {
            subView.alpha = 1;
        }
    }
    
    func animatedDisappear() {
        self.endEditing(true);
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0;
        }) { _ in
            self.superview?.sendSubview(toBack: self);
        }
    }
    
    static func viewFromNib(getLastItem: Bool = false) -> UIView {
        let nibName = self.description().components(separatedBy: ".").last!; //Must get the last part of the description behind "." because description might return something like "myBundle.MyViewClass" can cause a crash
        let nib = UINib.init(nibName: nibName, bundle: nil)
        let objectsFromNib = nib.instantiate(withOwner: nil, options: nil)
        if getLastItem {
            return objectsFromNib.last as! UIView
        }
        return objectsFromNib.first as! UIView
    }
    
    func roundedCorner(radius: CGFloat, borderWidth: CGFloat = 1, borderColor: UIColor = UIColor.clear) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        self.layer.borderWidth = borderWidth
        self.clipsToBounds = true
        self.layer.borderColor = borderColor.cgColor
    }
}

public enum Direction: Int {
    case Up
    case Down
    case Left
    case Right
    
    public var isX: Bool { return self == .Left || self == .Right }
    public var isY: Bool { return !isX }
}

public extension UIPanGestureRecognizer {
    
    public var direction: Direction? {
        let velocity = self.velocity(in: view)
        let vertical = fabs(velocity.y) > fabs(velocity.x)
        switch (vertical, velocity.x, velocity.y) {
        case (true, _, let y) where y < 0: return .Up
        case (true, _, let y) where y > 0: return .Down
        case (false, let x, _) where x > 0: return .Right
        case (false, let x, _) where x < 0: return .Left
        default: return nil
        }
    }
}
