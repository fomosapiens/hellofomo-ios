//
//  Alias.swift
//  Vaster
//
//  Created by Toan on 7/1/16.
//  Copyright © 2016 Elisoft. All rights reserved.
//

import UIKit

let resultKey = "result"
let messageKey = "message"
let resultOkKey = "ok"
let resultDataKey = "data"
let statusKey = "status"
let errorKey = "error"

let callDurationOut: Double = 60000


// Local notification
let localMissCallTypeValue = "missCall"

//Basic color
let basicColor = UIImage(named: "one")?.getPixelColor(CGPoint(x: 5, y: 5))
let  APP_MAIN_FONT = "OpenSans"

//Alert Statement
let notThingChange = "Nothing to change"


// Basic
let Empty_String = "--"
let Not_Available = "Not Available"

//StoryboardName
let Storyboard_Settings = "Settings"


//==================== USER CREDENTIALS ===========================

func USER_DEFAULT_SET(value:Any?, key: String) {
    UserDefaults.standard.set(value, forKey: key)
}

func USER_DEFAULT_GET(key: String) -> Any? {
    return UserDefaults.standard.object(forKey: key)
}

let OK_LOCALIZED = LOCALIZED_STRING("alert_dialog_ok")
let CANCEL_LOCALIZED = LOCALIZED_STRING("alert_dialog_cancel")
let YES_LOCALIZED = LOCALIZED_STRING("alert_dialog_yes")

let DASHBOARD_TYPE = "dashboardData"
let NEWS_TYPE = "newsData"
let CATEGORY_TYPE = "categoryData"

let NETWORK_LOST = "Your network connection has been lost. Please check the connection."

