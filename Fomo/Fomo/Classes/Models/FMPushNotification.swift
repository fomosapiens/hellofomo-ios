//
//  FMPushNotification.swift
//  Fomo
//
//  Created by Khoa Bui on 8/14/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import Foundation
import ObjectMapper

enum PushStatus: String {
    case newPost = "new_post"
}

class FMPushNotification: Mappable {
    var postId: Int?
    var postType: PushStatus?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        var postTypeIntTemp: Int?
        var postTypeStrTemp: String?
        postTypeIntTemp <- map["post_id"]
        postTypeStrTemp    <- map["post_id"]
        if let postTypeIntTemp = postTypeIntTemp {
            postId = postTypeIntTemp
        } else if let postTypeStrTemp = postTypeStrTemp {
            postId = postTypeStrTemp.toInt()
        }
        
        var postTypeTemp: String?
        postTypeTemp    <- map["type"]
        if let postTypeTemp = postTypeTemp {
            postType = PushStatus(rawValue: postTypeTemp)
        }
    }
}
