//
//  FMCategory.swift
//  Fomo
//
//  Created by Tuan Nguyen on 8/1/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import ObjectMapper

class FMCategory: NSObject, Mappable, NSCoding {
    var  id: Int?
    var  name: String?
    
    required init?(map: Map) {
        super.init()
    }
    
    func mapping(map: Map) {
        id   <- map["id"]
        name <- map["name"]
    }
    
    // NSCoding
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        self.id  = aDecoder.decodeInteger(forKey: "id")
        self.name  = aDecoder.decodeObject(forKey: "name") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        
        if let id = self.id {
            aCoder.encode(id, forKey: "id")
        }
        
        if let name = self.name {
            aCoder.encode(name, forKey: "name")
        }
    }
    
    func isFavorites () -> Bool {
        return (self.id == -1)
    }
    
    static func saveCategories(_ categories: [FMCategory]) {
        var result : [Data] = []
        for category in categories {
            let data = NSKeyedArchiver.archivedData(withRootObject: category)
            result.append(data)
        }
        
        USER_DEFAULT_SET(value: result, key: .categories)
    }
    
    static func getCategories() -> [FMCategory] {
        if let categories = USER_DEFAULT_GET(key: .categories) as? [Data] {
            var result : [FMCategory] = []
            for data in categories {
                let cat = NSKeyedUnarchiver.unarchiveObject(with: data) as! FMCategory
                result.append(cat)
            }
            
            let allCat = FMCategory()
            allCat.name = "Alle"
            allCat.id = 0
            result.insert(allCat, at: 0)
            let favCat = FMCategory()
            favCat.name = "Favoriten"
            favCat.id = -1
            result.insert(favCat, at: result.count)
            
            return result
        }
        else{
            return []
        }
    }
}
