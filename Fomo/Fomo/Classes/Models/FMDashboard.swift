//
//  FMDashboard.swift
//  Fomo
//
//  Created by Khoa Bui on 8/8/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper

enum LayoutType: String {
    
    case slider = "slider"
    case single = "single"
    case list = "list"
}

enum DataType: String {
    
    case category = "category"
    case tag = "tag"
    case all = "all"
}

class FMDashboard: Mappable {
    
    var sectionName: String?
    var sectionDescription: String?
    var sectionType: String?
    var layout: LayoutType?
    var showPicture: Bool?
    var type: DataType?
    var typeValue: String?
    var dataCount: String?
    var dataOffset: String?
    var ordering : String?
    var indexArticlesMapping:Int?
    var articles:[FMArticle]?
    var topNews: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        sectionName             <- map["section_name"]
        sectionDescription      <- map["section_description"]
        sectionType             <- map["section_type"]
        
        typeValue               <- map["type_value"]
        dataCount               <- map["data_count"]
        dataOffset              <- map["data_offset"]
        ordering                <- map["ordering"]
        topNews                 <- map["top_news"]

        var typeTemp: String?
        typeTemp              <- map["type"]
        if let typeTemp = typeTemp {
            type = DataType(rawValue: typeTemp)
        }
        
        var layoutTemp: String?
        layoutTemp              <- map["layout"]
        if let layoutTemp = layoutTemp {
            layout = LayoutType(rawValue: layoutTemp)
        }
        
        
        var showPictureStrTemp: String?
        var showPictureIntTemp: Bool?
        showPictureStrTemp <- map["show_picture"]
        showPictureIntTemp    <- map["show_picture"]
        if let showPictureStrTemp = showPictureStrTemp {
            if showPictureStrTemp == "1" {
                showPicture = true
            } else {
                showPicture = false
            }
        } else if let showPictureIntTemp = showPictureIntTemp {
            showPicture = showPictureIntTemp
        }
    }
}
