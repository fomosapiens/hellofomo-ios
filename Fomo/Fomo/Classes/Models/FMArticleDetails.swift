//
//  FMArticleDetails.swift
//  Fomo
//
//  Created by Khoa Bui on 8/8/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import Foundation
import ObjectMapper

class FMArticleDetails: FMArticle {
    
    var tags: [FMTag]?
    var categories: [FMCategory]?
    var galleries: [String]?
    var relatedNews: [FMArticle]?
    var authors : [FMAuthor]?
    var attachments : [String]?
    var layoutSettings: [FMPost]?
    var sharingText: String?
    var sharingUrl: String?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        tags            <- map["tags"]
        categories      <- map["categories"]
        galleries       <- map["galleries"]
        attachments     <- map["attachments"]
        relatedNews     <- map["related_news"]
        authors         <- map["authors"]
        sharingText     <- map["share_information.description"]
        sharingUrl      <- map["share_information.url"]
        
        url  <- map["url"]
        var layouts: [FMPost]?
        
        layouts  <- map["layout_settings"]
        layoutSettings = layouts?.filter {
            if $0.enable != false && $0.name != "tags" {
                return true
            }
            return false
        }
       
    }
}

class FMTag: Mappable  {
    
    var id: Int?
    var name: String?

    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        id      <- map["id"]
        name    <- map["name"]
    }
}

class FMAuthor: Mappable {
    var id: Int?
    var name: String?
    var photo : String?
    var url : String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        id      <- map["id"]
        name    <- map["name"]
        url     <- map["callback_url"]
        photo   <- map["primary_image"]
    }
}

class FMRelatedNews: Mappable  {
    
    var id: Int?
    var title: String?
    var image : String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        id      <- map["id"]
        title    <- map["title"]
    }
}

class FMAttachment: Mappable  {
    
    var id: Int?
    var name: String?
    var url : String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        
        id      <- map["id"]
        name    <- map["title"]
    }
}
