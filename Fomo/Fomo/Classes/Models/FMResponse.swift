//
//  FMResponse.swift
//  Fomo
//
//  Created by Tuan Nguyen on 8/2/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class FMResponse: NSObject {
    var code : Int?
    var message: String {
        get {
            if let code = self.code {
                if code == 2001 {
                    return "Số điện thoại này đã được đăng ký"
                }
                    //Login
                else if code == 1001 {
                    return "Số điện thoại hoặc mật khẩu không chính xác"
                }
            }
            return "Đã có lỗi xảy ra. Vui lòng thử lại sau"
        }
    }
    
    init(code: Int? = nil) {
        self.code = code
        super.init()
    }
}
