//
//  FMPost.swift
//  Fomo
//
//  Created by Quan Quach on 9/10/18.
//  Copyright © 2018 Elinext. All rights reserved.
//

import UIKit
import ObjectMapper

enum PostType: String {
    case RelatedNews = "related_news"
    case Gallery = "gallery"
    case Authors = "authors"
    case Files = "files"
}

class FMPost: NSObject, Mappable {
    var name: String?
    var enable: Bool?
    var postType: PostType?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["key"]
        enable <- map["value"]
        
        if let nameString = name {
            postType = PostType(rawValue: nameString)
        }
        
    }
}
