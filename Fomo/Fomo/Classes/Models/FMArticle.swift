//
//  FMArticle.swift
//  Fomo
//
//  Created by Khoa Bui on 8/8/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import Foundation
import ObjectMapper


class FMArticle: NSObject, Mappable ,NSCoding{
    
    var id: Int!
    var title: String?
    var introduction: String?
    var content: String?
    var availableDate: Date?
    var primaryImage: String?
    var thumbnailImage: String?
    var url: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        id              <- map["id"]
        title           <- map["title"]
        introduction    <- map["introduction"]
        content     <- map["description"]
        availableDate   <- map["available_date"]
        primaryImage    <- map["primary_image"]
        availableDate  <- (map["available_date"], CustomDateFormatTransform(formatString: "dd.MM.yyyy HH:mm"))
        thumbnailImage  <- map["thumbnail"]
        
    }
    
    func availableDateText() -> String? {
        if let availableDate = self.availableDate{
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "dd.MM.yyyy"
            return formatter.string(from: availableDate)
        }
        
        return nil
    }
    func detailDateText() -> String? {
        if let availableDate = self.availableDate{
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "dd.MM.yyyy | HH:mm"
            return formatter.string(from: availableDate)
        }
        
        return nil
    }
    
    // NSCoding
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        self.id  = aDecoder.decodeObject(forKey: "id") as? Int
        self.title  = aDecoder.decodeObject(forKey: "title") as? String
        self.introduction  = aDecoder.decodeObject(forKey: "introduction") as? String
        self.content  = aDecoder.decodeObject(forKey: "content") as? String
        self.availableDate  = aDecoder.decodeObject(forKey: "availableDate") as? Date
        self.primaryImage  = aDecoder.decodeObject(forKey: "primaryImage") as? String
        self.thumbnailImage = aDecoder.decodeObject(forKey: "thumbnailImage") as? String
        self.url = aDecoder.decodeObject(forKey: "url") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(introduction, forKey: "introduction")
        aCoder.encode(content, forKey: "content")
        aCoder.encode(availableDate, forKey: "availableDate")
        aCoder.encode(primaryImage, forKey: "primaryImage")
        aCoder.encode(thumbnailImage, forKey: "thumbnailImage")
        aCoder.encode(url, forKey: "url")
    }
}
