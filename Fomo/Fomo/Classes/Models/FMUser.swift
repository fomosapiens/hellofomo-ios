//
//  FMUser.swift
//  Fomo
//
//  Created by Tuan Nguyen on 8/16/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import ObjectMapper
class FMUser: NSObject, Mappable {
    
    var accessToken: String?
    var refreshToken: String?
    var expiredDate: Date?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        accessToken              <- map["accessToken"]
        refreshToken           <- map["refreshToken"]
        var expired: TimeInterval?
        expired <- map["expires_in"]
        if let expired = expired {
            self.expiredDate = Date(timeIntervalSince1970: expired)
        }
    }
}
