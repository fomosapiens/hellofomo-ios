//
//  FArticlesTableViewCell.swift
//  Fomo
//
//  Created by Khoa Bui on 7/3/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import SnapKit

class FArticlesTableViewCell: UITableViewCell {

    @IBOutlet weak var articlesImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var thumbImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var thumbImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var smallArticleStackView: UIStackView!
    
    @IBOutlet weak var lineView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setFont()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.lineView.snp.updateConstraints { (maker) in
            
            maker.leading.equalTo(15)
        }
    }
    
    func setData(_ data: FMArticle, forThumbnail: Bool = false, isFullLine: Bool = false, isBottomLineHidden: Bool = false) {
        self.dateLabel.text = data.availableDateText() ?? ""
        self.descriptionLabel.text = data.title ?? ""
        self.lineView.isHidden = isBottomLineHidden
        
        self.lineView.snp.updateConstraints { (maker) in
            
            maker.leading.equalTo(isFullLine ? 0:15)
        }
        
        if forThumbnail {
            self.articlesImageView.displayImage(data.thumbnailImage, placeholderImage: PLACEHOLDER_IMAGE)
        }
        else {
            self.articlesImageView.displayImage(data.primaryImage, placeholderImage: PLACEHOLDER_IMAGE)
        }
        
    }
    
    func showThumbnail(type: LayoutType, isShow: Bool = true) {
        switch type {
        case .slider:
            break
        case .single:
            if isShow {
                self.thumbImageHeightConstraint.constant = 150
            } else {
                self.thumbImageHeightConstraint.constant = 0
            }
        case .list:
            if isShow {
                self.smallArticleStackView.insertArrangedSubview(self.articlesImageView, at: 0)
                self.articlesImageView.isHidden = false
            } else {
                self.smallArticleStackView.removeArrangedSubview(self.articlesImageView)
                self.articlesImageView.isHidden = true
            }
        }
    }
    
    fileprivate func setFont() {
        self.dateLabel.font = UIFont.font(type: .Regular, size: 15)
        self.descriptionLabel.font = UIFont.font(type: .Regular, size: 15)
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override var layoutMargins: UIEdgeInsets {
        get { return .zero }
        set(newVal) {}
    }
}
