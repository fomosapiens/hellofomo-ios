//
//  FMSliderTableViewCell.swift
//  Fomo
//
//  Created by Khoa Bui on 8/10/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
import DTCoreText

class FMSliderTableViewCell: UITableViewCell {

    @IBOutlet weak var pageControl: FMPageControl!
    @IBOutlet weak var headerScrollView: UIScrollView!
    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var heightContentViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightContainerViewConstraint: NSLayoutConstraint!
    
    var didSelectArticle: CallBackEvent?
    var headerBannerViewArray:[FMHeaderBannerView] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.headerScrollView.delegate = self
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.pageControl.updateDots()
    }

    func setData(data: FMDashboard?) {
        if let showPic = data?.showPicture, showPic == true {
            self.heightContentViewConstraint.constant = 260
            self.heightContainerViewConstraint.constant = 300
        } else {
            self.heightContentViewConstraint.constant = 95
            self.heightContainerViewConstraint.constant = 135
        }
        headerBannerViewArray = []
        if let articles = data?.articles {
            for article in articles {
                var headerBannerView: FMHeaderBannerView!
                if let isShowPic = data?.showPicture, isShowPic == true {
                    headerBannerView = FMHeaderBannerView.viewFromNib() as! FMHeaderBannerView
                } else {
                    headerBannerView = FMHeaderBannerView.viewFromNib(getLastItem: true) as! FMHeaderBannerView
                }
                headerBannerView.setData(data: article)
                headerBannerView.didSelectArticle = {[weak self](article) -> () in
                    if let weakSelf = self {
                        weakSelf.didSelectArticle?(article)
                    }
                }
                self.headerBannerViewArray.append(headerBannerView)
            }
            self.loadBannerData(view: self.headerBannerViewArray, inView: self.headerScrollView)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    fileprivate func loadBannerData(view subViewArray: [UIView], inView view:UIView ) {
        self.pageControl.numberOfPages = subViewArray.count
        var lastSubView: UIView?
        for subView in subViewArray {
            view.addSubview(subView)

            // Set autolay constraint
            subView.snp.makeConstraints {(make) in

                make.top.bottom.equalTo(view)
                make.width.equalTo(SCREEN_WIDTH())
                if let lastSubView = lastSubView {
                    make.left.equalTo(lastSubView.snp.right)
                } else {
                    make.left.equalTo(view)
                }
                if subView == subViewArray.last {
                    make.right.equalTo(view)
                }
                lastSubView = subView
            }
        }
    }
}

extension FMSliderTableViewCell: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.headerScrollView == scrollView {
            let x = scrollView.contentOffset.x
            let w = scrollView.bounds.size.width
            self.pageControl.currentPage = Int(x/w)
            self.pageControl.updateDots()
        }
    }
}
