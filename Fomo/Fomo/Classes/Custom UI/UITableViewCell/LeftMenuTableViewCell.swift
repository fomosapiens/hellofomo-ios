//
//  FLeftMenuTableViewCell.swift
//  Fomo
//
//  Created by Khoa Bui on 7/3/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

protocol FMLeftMenuTableViewCellDelegate: class {
    func leftMenuCell(_ cell: FLeftMenuTableViewCell , didClickCollapse collapsed: Bool)
}

class FLeftMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collapseButton: UIButton!
     weak var delegate : FMLeftMenuTableViewCellDelegate?
    
    var isCollapsed: Bool = false {
        didSet {
            let title = (self.isCollapsed) ? "+" : "-"
            self.collapseButton.setTitle(title, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.titleLabel.font = UIFont.font(type: .Regular, size: 18)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func collapseButtonClicked(_ sender: Any) {
        self.isCollapsed = !self.isCollapsed
        self.delegate?.leftMenuCell(self, didClickCollapse: self.isCollapsed)
        let title = (self.isCollapsed) ? "+" : "-"
        self.collapseButton.setTitle(title, for: .normal)
    }
}
