//
//  HeadlineTableViewCell.swift
//  Fomo
//
//  Created by Khoa Bui on 8/7/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit

class HeadlineTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        // Initialization code
    }
    
    func setData(data: FMDashboard?) {
        self.titleLabel.text = data?.sectionDescription ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
