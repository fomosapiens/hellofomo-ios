//
//  ThinLine.swift
//  User-iOS
//
//  Created by Tuan Nguyen on 4/25/17.
//  Copyright © 2017 com.order. All rights reserved.
//

import UIKit

class ThinLine: UIView {
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        if let context = UIGraphicsGetCurrentContext() {
            context.setLineWidth(0.5)
            //start at this point
            context.move(to: CGPoint(x: 0, y: self.frame.size.height))
            //draw to this point
            context.addLine(to: CGPoint(x: self.frame.size.width, y: self.frame.size.height))
            context.setStrokeColor((backgroundColor?.cgColor)!)
            // and now draw the Path!
            context.strokePath()
        }
    }
}
