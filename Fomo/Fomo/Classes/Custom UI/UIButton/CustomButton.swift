//
//  CustomButton.swift
//  Fomo
//
//  Created by Khoa Bui on 7/10/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomButton: UIButton {
    
    fileprivate struct Constants {
        static let defaultiActiveColor = UIColor.colorWithHexString("#000000")
        static let defaultiDeactiveColor = UIColor.colorWithHexString("#AAAAAA")
    }
    
    @IBInspectable var activeColor: UIColor = Constants.defaultiActiveColor
    @IBInspectable var deactiveColor: UIColor = Constants.defaultiDeactiveColor
    @IBInspectable var isActivate: Bool = true {
        didSet {
            if isActivate {
                setActiveColor()
            } else {
                setDeactiveColor()
            }
        }
    }
    
    func setActiveColor() -> Void {
        UIView.animate(withDuration: 0.5) { 
            self.setTitleColor(self.activeColor, for: UIControlState())
            self.layer.borderWidth = 0
        }
    }
    
    func setDeactiveColor() -> Void {
        UIView.animate(withDuration: 0.5) {
            self.setTitleColor(self.deactiveColor, for: UIControlState())
            self.layer.borderWidth = 0
        }
    }
}
