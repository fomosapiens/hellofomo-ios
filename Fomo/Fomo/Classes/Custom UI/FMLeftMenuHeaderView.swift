//
//  FMLeftMenuHeaderView.swift
//  Fomo
//
//  Created by Tuan Nguyen on 9/18/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import UIKit
protocol FMLeftMenuHeaderViewDelegate: class {
    func leftMenuHeaderView(_ view: FMLeftMenuHeaderView , didClickCollapse collapsed: Bool)
}
class FMLeftMenuHeaderView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collapseButton: UIButton!
    weak var delegate : FMLeftMenuHeaderViewDelegate?
    
    var isSelected : Bool = false
    var isCollapsed: Bool = false
    
    override func awakeFromNib() {
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
//        self.addGestureRecognizer(tapGesture)
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleTapGesture))
        gesture.minimumPressDuration = 0.01
        self.addGestureRecognizer(gesture)
    }
    
    func handleTapGesture (_ gesture : UIGestureRecognizer) {
        if gesture.state == .began {
            self.isSelected = !self.isSelected
            self.backgroundColor = (self.isSelected) ? UIColor.lightGray : UIColor.white
        }
        else if gesture.state == .cancelled || gesture.state == .ended {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: { 
                self.isSelected = !self.isSelected
                self.backgroundColor = (self.isSelected) ? UIColor.lightText : UIColor.white
            })
        }
    }
    
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.isSelected = !self.isSelected
//        self.backgroundColor = (self.isSelected) ? UIColor.lightGray : UIColor.white
//    }
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.isSelected = !self.isSelected
//        self.backgroundColor = (self.isSelected) ? UIColor.lightGray : UIColor.white
//    }
//    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.isSelected = !self.isSelected
//        self.backgroundColor = (self.isSelected) ? UIColor.lightGray : UIColor.white
//    }
    
    
    @IBAction func collapseButtonClicked(_ sender: Any) {
        self.isCollapsed = !self.isCollapsed
        self.delegate?.leftMenuHeaderView(self, didClickCollapse: self.isCollapsed)
        let title = (self.isCollapsed) ? "+" : "-"
        self.collapseButton.setTitle(title, for: .normal)
    }
}
