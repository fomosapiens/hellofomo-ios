//
//  VONotificationView.swift
//  User-iOS
//
//  Created by Tuan Nguyen on 5/16/17.
//  Copyright © 2017 com.order. All rights reserved.
//

import UIKit
enum VONotificationMessageType : Int{
    case success
    case failure
    case warning
}

let NOTIFICATION_VIEW = VONotificationView.sharedInstance
let CLOSE_BUTTON_IMAGE = UIImage(named: "ic_close_white")
class VONotificationView: NSObject {
    static let sharedInstance = VONotificationView()
    private var statusBarStyle: UIStatusBarStyle?
    private var notificationViews: [UIView]?
    
    // MARK: -
    func isOnScreen() -> Bool {
        if let views = self.notificationViews {
            return (views.count > 0) ? true : false
        }
        return false
    }
    
    func setStatusBarStyle(style: UIStatusBarStyle) {
        self.statusBarStyle = style
    }
    
    // MARK: -
    func showSuccess(message: String?, duration: Int = 7) {
        if let message = message {
            let view = self.notificationView(withMessage: message, type: .success)
            self.showNotificationView(view, duration: duration)
        }
        
    }
    func showFailure(message: String?, duration: Int = 7) {
        if let message = message {
            let view = self.notificationView(withMessage: message, type: .failure)
            self.showNotificationView(view, duration: duration)
        }
    }

    // MARK: - Action
    @objc private func dismissButtonClicked(sender: UIButton) {
        if let superView = sender.superview {
            self.dismiss(view: superView, shouldChangeStatusBarStyle: true)
        }
    }
    
    // MARK: - Show/hide
    private func showNotificationView(_ view: UIView, duration: Int) {
        view.setY(newY: -view.height())
        UIApplication.currentAppDelegate().window?.addSubview(view)
        
        if self.notificationViews == nil {
            self.notificationViews = []
        }
        
        if self.notificationViews?.count == 0 {
            self.statusBarStyle = UIApplication.shared.statusBarStyle
        }
        
        UIView.animate(withDuration: 0.3) {
            view.setY(newY: 0)
            UIApplication.shared.statusBarStyle = .lightContent
            
            for notificationView in self.notificationViews! {
                notificationView.alpha = 0
            }
        }
        
        self.notificationViews?.append(view)
        DispatchQueue.main.asyncAfter(deadline: (.now() + TimeInterval(duration))) {
            self.dismiss(view: view)
        }
    }
    
    private func dismiss (view: UIView, shouldChangeStatusBarStyle change: Bool = false) {
        UIView.animate(withDuration: 0.3, animations: {
            view.setY(newY: (-view.height()))
        }, completion: { (succeeded) in
            view.removeFromSuperview()
//            self.notificationViews?.remove(<#T##item: UIView##UIView#>)
            self.notificationViews?.remove(view)
            
            if let style = self.statusBarStyle {
                if change {
                    UIApplication.shared.statusBarStyle = style
                }
                else if self.notificationViews?.count == 0 {
                    UIApplication.shared.statusBarStyle = style
                }
            }
            
        })
    }
    
    // MARK: -
    private func notificationView(withMessage message: String, type: VONotificationMessageType) -> UIView {
        let labelTopLeftMargin: CGFloat  = 10.0
        let labelRightMargin: CGFloat = 5.0
        let statusBarHeight: CGFloat = 20.0
        let dismissButtonSize: CGFloat = 50.0
        let textWidth = SCREEN_SIZE.width - labelTopLeftMargin - dismissButtonSize - labelRightMargin
        let font = UIFont(name: "Roboto-Bold", size: 16)
        let textHeight = message.height(withConstrainedWidth: textWidth, font: font!)
        
        let height = max(64, (textHeight + 2 * labelTopLeftMargin + statusBarHeight))
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: height))
        
        if type == .failure {
            view.backgroundColor = UIColor.red
        }
        else {
            view.backgroundColor = UIColor(hex: ML_BLUE_COLOR)
        }
        
        let contentLabel = UILabel(frame: CGRect(x: labelTopLeftMargin, y: statusBarHeight, width: textWidth, height: (height - statusBarHeight - labelTopLeftMargin)))
        contentLabel.font = font
        contentLabel.numberOfLines = 0
        contentLabel.lineBreakMode = .byWordWrapping
        contentLabel.text = message
        contentLabel.textColor = UIColor.white
        
        let dismissButton = UIButton(frame: CGRect(x: view.width() - dismissButtonSize, y: labelTopLeftMargin, width: dismissButtonSize, height: view.height() - labelTopLeftMargin))
        dismissButton.addTarget(self, action: #selector(dismissButtonClicked), for: .touchUpInside)
        dismissButton.setImage(CLOSE_BUTTON_IMAGE, for: .normal)

        view.addSubview(dismissButton)
        view.addSubview(contentLabel)
        
        return view
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
}
