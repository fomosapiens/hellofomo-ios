//
//  VOUnderlineButton.swift
//  User-iOS
//
//  Created by Tuan Nguyen on 5/24/17.
//  Copyright © 2017 com.order. All rights reserved.
//

import UIKit

class FMUnderlineButton: UIButton {

    override func draw(_ rect: CGRect) {
        // Drawing code
        if let context = UIGraphicsGetCurrentContext() {
            if let titleLabel = self.titleLabel {
                let textRect = titleLabel.frame
                
                // need to put the line at top of descenders (negative value)
                let descender = titleLabel.font.descender + 3
                
                // set to same colour as text
                context.setStrokeColor(titleLabel.textColor.cgColor)
                context.move(to: CGPoint(x: textRect.origin.x, y: textRect.origin.y + textRect.size.height + descender))
                context.addLine(to: CGPoint(x: textRect.origin.x + textRect.size.width, y: textRect.origin.y + textRect.size.height + descender))

                context.closePath()
                context.drawPath(using: .stroke)
            }
        }
    }
    
}
