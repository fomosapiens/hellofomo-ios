//
//  FMLayoutManager.swift
//  Fomo
//
//  Created by Khoa Bui on 7/10/17.
//  Copyright © 2017 Elinext. All rights reserved.
//

import Foundation
import UIKit

typealias DidCompleteDownloadDashboardDataSource = (_ isSuccess: Bool) -> Void

let LAYOUT_MANAGER = FMLayoutManager.shared

final class FMLayoutManager {
    
    // Properties
    var dataLayout: [String: [FMDashboard]] = [:]
    var dashboardData: [FMDashboard] = []
    var newsData: [FMDashboard] = []
    var categoryData: [FMDashboard] = []
    var didCompleteDownloadDashboardData: DidCompleteDownloadDashboardDataSource?
    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    static let shared = FMLayoutManager()

    
    func createLayoutDashboard(layout: FLayoutType = .Dashboard,
                               completion: ((_ isSuccess: Bool?) -> Void)?) {
        self.dashboardData = []
        self.newsData = []
        
        if layout != .Category {
            API_MANAGER.requestGetDataLayoutInfo(layout: layout, success: { datasLayout in
                let group = DispatchGroup()
                
                if let datasLayout = datasLayout {
//                    let newDatasLayout = datasLayout.filter {
//                        $0.type != nil
//                    }
                    for (index, dataLayout) in datasLayout.enumerated() {
//                        print(dataLayout.topNews)
                        guard let typeTemp = dataLayout.type else { completion?(false); return }
                        let typeValue = dataLayout.typeValue
                        var categories: String?
                        var tags: String?
                        if typeTemp == .category && typeValue != "null" && typeValue != nil {
                            categories = typeValue
                        } else if typeTemp == .tag {
                            tags = typeValue
                        }
                        
                        
                        group.enter()
                        
                        dataLayout.indexArticlesMapping = index
                        
                        API_MANAGER.requestGetNewsListing(lang: FMLanguage.de.rawValue, ofDashboard: dataLayout, categories: categories, tags: tags, limit: dataLayout.dataCount?.toInt(), page: nil, offset: (dataLayout.dataOffset?.toInt())!, success: { (newsArticles, dataLayout) in
                            
                            if let newsArticles = newsArticles,
                                let dataLayout = dataLayout  {
                                dataLayout.articles = newsArticles
                                if layout == .Dashboard {
                                    self.dashboardData.append(dataLayout)
                                } else if layout == .News {
                                    self.newsData.append(dataLayout)
                                }
                            }
                            group.leave()
                            
                        }, failure: { error in
                            // TODO:- Will update late
                            group.leave()
                            //                        completion?(false)
                        })
                    }
                } else {
                    completion?(false)
                }
                
                group.notify(queue: DispatchQueue.main) {
                    DispatchQueue.main.async {
                        if layout == .Dashboard {
                            self.clearData(layout, forKey: DASHBOARD_TYPE)
                            self.dashboardData = self.dashboardData.sorted(by: { $0.indexArticlesMapping! < $1.indexArticlesMapping! })
                            self.dataLayout.updateValue(self.dashboardData, forKey: DASHBOARD_TYPE)
                        } else if layout == .News {
                            self.clearData(layout, forKey: NEWS_TYPE)
                            self.newsData = self.newsData.sorted(by: { $0.indexArticlesMapping! < $1.indexArticlesMapping! })
                            self.dataLayout.updateValue(self.newsData, forKey: NEWS_TYPE)
                        }
                        
                        // TODO:- Noti get data success
                        completion?(true)
                    }
                }
                
            }) { error in
                // TODO:- Will update late
                completion?(false)
            }
        }
        
    }
    
    func handlePushNotification(withUserInfo: [String: Any]) {
        API_MANAGER.mappingPushNotificationData(withUserInfo) { (notificationData) in
            if notificationData != nil {
                let  navigationController = BaseNavigationController()
                let dashBoardVC = FMDashboardViewController.newInstance()
                navigationController.viewControllers = [dashBoardVC]
            
            
                if let serviceVC = UIApplication.currentAppDelegate().window?.visibleViewController as? FMServiceViewController {
                    serviceVC.dismiss(animated: false, completion: {() -> () in
                        UIApplication.currentAppDelegate().window?.visibleViewController?.slideMenuController()?.changeMainViewController(navigationController, close: true)
                        
                        let articleDetailsVC = FMArticleDetailsViewController.initWithStoryboard()
                        articleDetailsVC.articleId = notificationData?.postId
                        dashBoardVC.navigationController?.pushViewController(articleDetailsVC, animated: true)
                    })
                } else {
                    UIApplication.currentAppDelegate().window?.visibleViewController?.slideMenuController()?.changeMainViewController(navigationController, close: true)
                    
                    let articleDetailsVC = FMArticleDetailsViewController.initWithStoryboard()
                    articleDetailsVC.articleId = notificationData?.postId
                    dashBoardVC.navigationController?.pushViewController(articleDetailsVC, animated: true)
                }
            }
            
            
            
        }
    }
    
    func clearData(_ layout: FLayoutType, forKey: String) {
        if layout == .Dashboard {
            self.dataLayout.removeValue(forKey: forKey)
        } else if layout == .News {
            self.dataLayout.removeValue(forKey: forKey)
        } else {
            self.dataLayout.removeValue(forKey: forKey)
        }
    }
    
    func getCategoryData(_ categoryId: Int, completion: ((_ isSuccess: Bool?) -> Void)?) {
        var categoryData = [FMDashboard]()
        
        API_MANAGER.requestGetDataLayoutInfo(layout: .Category, success: { datasLayout in
            let group = DispatchGroup()
            
            if let datasLayout = datasLayout {
                let newDatasLayout = datasLayout.filter {
                    $0.type != nil
                }
                for (index, dataLayout) in newDatasLayout.enumerated() {
//                    print(dataLayout.type)
                    //guard let typeTemp = dataLayout.type else { completion?(false); return }
                    //let typeValue = dataLayout.typeValue
                    let categories: String = String(categoryId)
                    //var tags: String?
                    
                    
                    
                    group.enter()
                    
                    dataLayout.indexArticlesMapping = index
                    
                    API_MANAGER.requestGetNewsListing(lang: FMLanguage.de.rawValue, ofDashboard: dataLayout, categories: categories, tags: nil, limit: dataLayout.dataCount?.toInt(), page: nil, offset: (dataLayout.dataOffset?.toInt())!, success: { (newsArticles, dataLayout) in
                        
                        if let newsArticles = newsArticles,
                            let dataLayout = dataLayout  {
                            dataLayout.articles = newsArticles
                            
                            categoryData.append(dataLayout)
                        }
                        group.leave()
                        
                    }, failure: { error in
                        // TODO:- Will update late
                        group.leave()
                        //                        completion?(false)
                    })
                }
            } else {
                completion?(false)
            }
            
            group.notify(queue: DispatchQueue.main) {
                DispatchQueue.main.async {
                        self.clearData(.Category, forKey: String(categoryId))
                        categoryData = categoryData.sorted(by: { $0.indexArticlesMapping! < $1.indexArticlesMapping! })
                        self.dataLayout.updateValue(categoryData, forKey: String(categoryId))
                    // TODO:- Noti get data success
                    completion?(true)
                }
            }
            
        }) { error in
            // TODO:- Will update late
            completion?(false)
        }
    }
}
