//
//  APIManager.swift
//  Prost
//
//  Created by Tuan Nguyen on 5/25/17.
//  Copyright © 2017 Tuan Nguyen. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftDate

let API_SERVER_URL = "https://hellofomo.rgc-manager.de/api/v1"
private let FM_CLIENT_ID = 3
private let FM_CLIENT_SECRET = "4xILYSlmwCCpnWu6ohvDsSB1MdYzXYYdvKC46z9a"

//private let FM_CLIENT_ID = 3
//private let FM_CLIENT_SECRET = "4xILYSlmwCCpnWu6ohvDsSB1MdYzXYYdvKC46z9a"
//let API_SERVER_URL = "http://intranet.ritter-gent.de/api/v1"

enum ErrorFromResponse: Int, Error, CustomStringConvertible {
    case lostInternet = -1
    
    var description: String {
        switch self {
        case .lostInternet:
            return "Please check your internet connection"
        default:
            return "Sonmething went wrong, please try again"
        }
    }
}


enum FMAPIPath : String {
    
    case login = "/auth"
    case updateToken = "/auth/refresh-token"
    case getCategories = "/categories"
    case logout = "/users/logout"
    case dashboard = "/layout/dashboard"
    case newsLayout = "/layout/news"
    case categoryLayout = "/layout/category"
    case contact = "/layout/contact"
    case privacy = "/layout/privacy"
    case impressum = "/layout/impressum"
    case agb = "/layout/agb"
    case news = "/news"
    case getFont = "/settings/font"
    
    func fullUrl() -> String  {
        return "\(API_SERVER_URL)\(self.rawValue)";
    }
}

let API_MANAGER = APIManager.sharedInstance

class APIManager: NSObject {
    static let sharedInstance = APIManager()
    
    func requestRegisterDevice(success : ((String?) -> Void)?, failure: ((FMResponse?) -> Void)?) {

        var params = ["client_id" : FM_CLIENT_ID,"client_secret" : FM_CLIENT_SECRET, "device_id": DEVICE_ID, "agent" : "ios"] as [String : Any]
        
        if let deviceToken = USER_DEFAULT_GET(key: .deviceToken) {
            params["fcm_token"] = deviceToken
        }
        
        self.REQUEST_POST(FMAPIPath.login.fullUrl(), parameters: params, success: { (response) in
            let token = response["access_token"] as? String
            let expiredIn = response["expires_in"] as? Int
            
            if let expiredIn  = expiredIn {
                let date = Date() + expiredIn.seconds
                USER_DEFAULT_SET(value: date, key: .tokenExpireDate)
            }
            
            USER_DEFAULT_SET(value: token, key: .userToken)
            success?(token)
        }) { (error) in
            failure?(error)
        }
    }
    
    func requestUpdateToken() {
        
        let deviceToken = USER_DEFAULT_GET(key: .deviceToken) ?? ""
        
        let params = ["client_id" : FM_CLIENT_ID,"client_secret" : FM_CLIENT_SECRET, "refresh_token" : deviceToken] as [String : Any]
        
        self.REQUEST_POST(FMAPIPath.updateToken.fullUrl(), parameters: params, success: { (response) in
            
        }) { (error) in
            
        }
    }
    
    func requestGetCategories(withCompletion completion:@escaping ([FMCategory]?) -> Void) {
        self.REQUEST_GET(FMAPIPath.getCategories.fullUrl(), success: { (response) in
            let categories = Mapper<FMCategory>().mapArray(JSONObject: response["data"])
            completion(categories)
        }) { (error) in
            completion(nil)
        }
    }
    
    func requestGetService(type: FMServiceType, completion:@escaping (String?) -> Void) {
        var url = FMAPIPath.contact.fullUrl()
        if type == .privacy {
            url = FMAPIPath.privacy.fullUrl()
        }
        else if type == .impressum {
            url = FMAPIPath.impressum.fullUrl()
        }
        else if type == .agb {
            url = FMAPIPath.agb.fullUrl()
        }
        
        self.REQUEST_GET(url, success: { (response) in
            completion(response["data"] as? String)
        }) { (error) in
            completion(nil)
        }
    }
    
    func requestGetAppFont(withCompletion completion:@escaping (String?) -> Void) {
        self.REQUEST_GET(FMAPIPath.getFont.fullUrl(), success: { (response) in
            completion(response["data"] as? String)
        }) { (error) in
            completion(nil)
        }
    }
    
    // MARK: - Get dashboard infomation
    /**
     Get dashboard infomation
     - success: callback success with FMDashboard array
     - failure: callback when something with error
     - returns: Void
     */
    
    func requestGetDataLayoutInfo(layout: FLayoutType,
                                 success : (([FMDashboard]?) -> Void)?,
                                 failure: ((FMResponse?) -> Void)?) {
        var url = String()
        if layout == .Dashboard {
            url = FMAPIPath.dashboard.fullUrl()
        } else if layout == .News  {
            url = FMAPIPath.newsLayout.fullUrl()
        }
        else {
            url = FMAPIPath.categoryLayout.fullUrl()
        }
        
        self.REQUEST_GET(url, success: { (response) in
            var dashboards: [FMDashboard]?
            if let dashboardArray = response["data"] as? [[String:Any]] {
                dashboards = Mapper<FMDashboard>().mapArray(JSONArray: dashboardArray)
            }
            success?(dashboards)
        }) { (error) in
            failure?(nil)
        }
    }
    
    // MARK: - Get News Listing
    /**
     Get News Listing
     - lang : en or de, Default 'de' (German)
     - categories : category IDs separated by commas, e.g. 1,2,3
     - tags : tag IDs separated by commas, e.g. 1,2,3
     - limit : Limiting the number of items
     - page : Pagination page
     - success: callback success with FMArticle array
     - failure: callback when something error
     - returns: Void
     */
    
    func requestGetNewsListing(lang: String = "de",
                               ofDashboard: FMDashboard?,
                               categories: String?,
                               tags: String?,
                               limit: Int?,
                               page: Int?,
                               offset: Int = -1,
                               success : (([FMArticle]?, FMDashboard?) -> Void)?,
                               failure: ((FMResponse?) -> Void)?) {
        
        var params = [ "lang"       : lang,
                       "limit"      : limit ?? 0] as [String: Any]
        if offset == -1{
            if let page = page {
                params["page"] = page
            }
        }
        else {
            params["offset"] = offset
        }
        
        if let categories = categories, let dashboard = ofDashboard, let type = dashboard.type {
            params["categories"] = categories
        }
        if let tags = tags {
            params["tags"] = tags
        }
        if let dashboard = ofDashboard, let topNews = dashboard.topNews, let topNewsNumber = Int(topNews) {
            params["top_news"] = topNewsNumber
        }
        self.REQUEST_GET(FMAPIPath.news.fullUrl(), parameters: params, success: { (response) in
            var newsListings : [FMArticle]?
            
//            print("params = \(params)" )
            if let newsListingArray = response["data"] as? [[String:Any]] {
                newsListings = Mapper<FMArticle>().mapArray(JSONArray: newsListingArray)
            }
            success?(newsListings/*?.filter { $0.availableDate != nil }*/, ofDashboard)
        }) { (error) in
            failure?(nil)
        }
    }
    
    // MARK: - Get Single News
    /**
     Get News Listing
     - lang : en or de, Default 'de' (German)
     - id : news ID
     - success: callback success with FMNewsDetails array
     - failure: callback when something error
     - returns: Void
     */
    
    func requestGetNewsDetail(lang: String = "de",
                               id: Int,
                               success : ((FMArticleDetails?) -> Void)?,
                               failure: ((FMResponse?) -> Void)?) {
        
        let params = [ "lang" : lang] as [String: Any]
        let url = FMAPIPath.news.fullUrl() + "/" + String(describing: id)
        self.REQUEST_GET(url, parameters: params, success: { (response) in
            let newsDetails = FMArticleDetails(JSON: (response["data"] as? [String : Any])!)
            success?(newsDetails)
        }) { (error) in
            failure?(error)
        }
    }
    
    // MARK: - Get category list
    /**
        Get category list
     - success: callback success with FMCategory array
     - failure: callback when something error
     - returns: Void
     */
    
    func requestGetFMCategories(success : (([FMCategory]?) -> Void)?,
                               failure: ((FMResponse?) -> Void)?) {
        
        self.REQUEST_GET(FMAPIPath.getCategories.fullUrl(), success: { (response) in
            
            var fmCategories : [FMCategory]?
            if let fmCategoriesArray = response["data"] as? [[String:Any]] {
                fmCategories = Mapper<FMCategory>().mapArray(JSONArray: fmCategoriesArray)
            }
            success?(fmCategories)
        }) { (error) in
            failure?(nil)
        }
    }
    
    func mappingPushNotificationData(_ userInfo: [String: Any],
                                     completion: ((FMPushNotification?) -> Void)?) {
        if let data = userInfo["data"] as? String {
            if let dataDict = data.parseJSONString as? [String: Any] {
                let pushNotification = FMPushNotification(JSON: dataDict)
                completion?(pushNotification)
                return
            }
        }
        completion?(nil)
    }
}

private let API_STATUS_SUCCESS = 200
private let API_ERROR_STATUS = false
private let API_ERROR_DOMAIN = "com.abc.error"



extension APIManager {
    fileprivate func REQUEST_GET(_ url:String, parameters:Parameters? = nil,
                    success: @escaping ([String:Any]) -> Void ,
                    failure: ((FMResponse?) -> Void)?) {
        self.request(url, parameters: parameters, success: success, failure: failure)
    }
    
    fileprivate func REQUEST_POST(_ url:String, parameters:Parameters? = nil,
                     success: @escaping ([String:Any]) -> Void ,
                     failure: ((FMResponse?) -> Void)?) {
        
        
        self.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, success: success, failure: failure)
    }
    
    fileprivate func request(_ url:String, method: HTTPMethod = .get, parameters:Parameters? = nil,encoding:ParameterEncoding = URLEncoding.default,
                 success: @escaping ([String:Any]) -> Void ,
                 failure: ((FMResponse?) -> Void)?) {
        
        var headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json"
            ]
        
        if let params = parameters {
            logInfo("Request params: \(params.description)")
        }

        if let token = USER_DEFAULT_GET(key: .userToken) as? String {
            headers["Authorization"] = "Bearer \(token)"
        }
        
        Alamofire.request(url, method: method, parameters:parameters,encoding: encoding, headers:headers).responseJSON { (response) in
            
            if (response.result.isSuccess) {
                logInfo(response.result.value.debugDescription)
                if let result = response.result.value as? [String:Any] {
                    if let error = result["errors"] as? Bool {
                        if (error == API_ERROR_STATUS) {
                            success(result)
                        } else {
                            let code = result["code"] as? Int
                            failure?(FMResponse(code: code))
                        }
                    }
                    else {
                        success(result)
                    }
                }
                else {
                    failure?(nil)
                }
            } else {
                let error = response.error as NSError?
                if let data = response.data {
                    let str = String.init(data: data, encoding: .utf8)
                    logError("\(url): \(str)")
                } else {
                    logError("\(url): \(String(describing: error?.localizedDescription))")
                }
                failure?(FMResponse(code: error?.code))
            }
        }
    }
    
    /// conveniently return an error with code
    ///
    /// - Parameter code: error code
    /// - Returns: the needed error
    private func errorWithCode(_ code: Int) -> NSError {
        return NSError(domain: API_ERROR_DOMAIN, code: code, userInfo: nil);
    }
}
